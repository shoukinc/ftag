package b2infosoft.achariya.com.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import b2infosoft.achariya.com.BuildConfig;
import b2infosoft.achariya.com.R;
import b2infosoft.achariya.com.global.AppGlobal;
import b2infosoft.achariya.com.pojo.ClassesPojo;
import b2infosoft.achariya.com.pojo.UserRegisterParceble;
import b2infosoft.achariya.com.useful.ConnectionDetector;
import b2infosoft.achariya.com.useful.RealPathUtil;
import b2infosoft.achariya.com.useful.UtilityMethod;
import b2infosoft.achariya.com.webservice.WebServiceCaller;

public class RegisterDocumentUploadActivity extends AppCompatActivity implements View.OnClickListener {
    Context mContext;
    Toolbar toolbar;
    TextView toolbar_title;
    TextView tvRcBooxTxt, tvDescTxt, tvRcBackTxt, tvRcFrontTxt, tvAadharTxt;
    ImageView imgRcBack, imgRcFront, imgAadhar;
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private SharedPreferences permissionStatus;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 101;
    private static final int GALLERY_INTENT_CALLED = 201;
    private String imageView = "";
    private boolean sentToSettings = false;
    String imgPathRcBack = "", imgPathAadhar = "", imgPathRcFront = "";
    ConnectionDetector connectionDetector;
    private static final String TAG = "UploadDocs";
    String Email = "", DOB = "", Pincode = "", class_id = "";
    UserRegisterParceble userRegisterParceble;
    Button btnSubmit;
    Spinner spinClass;
    EditText edtPrice, edtVechileNumber;
    String className = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acharya_activity_register_document_upload);
        mContext = RegisterDocumentUploadActivity.this;
        initView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @SuppressLint("LongLogTag")
    private void initView() {
        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Upload Doucument");
        toolbar_title.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        connectionDetector = new ConnectionDetector(mContext);
        spinClass = (Spinner) findViewById(R.id.spinClass);
        tvRcBooxTxt = (TextView) findViewById(R.id.tvRcBooxTxt);
        tvDescTxt = (TextView) findViewById(R.id.tvDescTxt);
        tvRcBackTxt = (TextView) findViewById(R.id.tvRcBackTxt);
        tvRcFrontTxt = (TextView) findViewById(R.id.tvRcFrontTxt);
        tvAadharTxt = (TextView) findViewById(R.id.tvAadharTxt);
        edtPrice = (EditText) findViewById(R.id.edtPrice);
        edtVechileNumber = (EditText) findViewById(R.id.edtVechileNumber);
        tvRcBooxTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvDescTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvRcBackTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvRcFrontTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvAadharTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        edtVechileNumber.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        edtPrice.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));

        imgRcBack = (ImageView) findViewById(R.id.imgRcBack);
        imgRcFront = (ImageView) findViewById(R.id.imgRcFront);
        imgAadhar = (ImageView) findViewById(R.id.imgAadhar);

        imgAadhar.setOnClickListener(this);
        imgRcFront.setOnClickListener(this);
        imgRcBack.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        Intent intent = getIntent();
        if (intent != null) {
            userRegisterParceble = (UserRegisterParceble) intent.getParcelableExtra("UserRegister");
            Log.d("getAadhar", userRegisterParceble.getAadhar());
            Log.d("getDob", userRegisterParceble.getDob());
        }
        if (connectionDetector.isConnectingToInternet()) {
            ClassesPojo.getClassesList(mContext, "RegisterDocumentUpload");
        } else {
            UtilityMethod.showAlertBox(mContext, "You have not connected to internet. Please Connect to internet and try again!");
        }


    }

    @Override
    public void onBackPressed() {
        UtilityMethod.goNextClass(mContext, AcharyaDeshBoardActivity.class);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgRcBack:
                imageView = "imgRcBack";
                permissionAccess();
                break;
            case R.id.imgAadhar:
                imageView = "imgAadhar";
                permissionAccess();
                break;
            case R.id.imgRcFront:
                imageView = "imgRcFront";
                permissionAccess();
                break;
            case R.id.btnSubmit:

                if (connectionDetector.isConnectingToInternet()) {
                    if (edtVechileNumber.getText().toString().trim().equals("")) {
                        UtilityMethod.showAlertBox(mContext, "Please Enter Vehicle Number.");
                    } else if (class_id.trim().equals("")) {
                        UtilityMethod.showAlertBox(mContext, "Please Select Class.");
                    } else if (imgPathRcFront.equals("")) {
                        UtilityMethod.showAlertBox(mContext, "Please Select RC(Front) Image.");
                    } else if (imgPathRcBack.equals("")) {
                        UtilityMethod.showAlertBox(mContext, "Please Select RC(Back) Image.");

                    } else if (imgPathAadhar.equals("")) {
                        UtilityMethod.showAlertBox(mContext, "Please Select Aadhar Card Image.");

                    } else {
                        //submit Request
                        Log.d(TAG, "onClick: " + userRegisterParceble.getMobileNumber());
                        Log.d(TAG, "onClick: " + userRegisterParceble.getFirst_name());
                        Log.d(TAG, "onClick: " + userRegisterParceble.getLast_name());
                        Log.d(TAG, "onClick: " + userRegisterParceble.getGender());
                        Log.d(TAG, "onClick: " + userRegisterParceble.getAddress());
                        Log.d(TAG, "onClick: " + userRegisterParceble.getAadhar());
                        Log.d(TAG, "onClick: " + userRegisterParceble.getEmail());
                        Log.d(TAG, "onClick: " + userRegisterParceble.getDob());
                        Log.d(TAG, "onClick: " + userRegisterParceble.getState());
                        Log.d(TAG, "onClick: " + userRegisterParceble.getCity());
                        Log.d(TAG, "onClick: " + userRegisterParceble.getPincode());
                        Log.d(TAG, "onClick: " + class_id);
                        Log.d(TAG, "onClick: " + edtVechileNumber.getText().toString());
                        Log.d(TAG, "onClick: " + edtPrice.getText().toString());
                        Log.d(TAG, "onClick: " + imgPathRcFront.toString());
                        Log.d(TAG, "onClick: " + imgPathRcBack.toString());
                        Log.d(TAG, "onClick: " + imgPathAadhar.toString());
                        WebServiceCaller addCarCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait...", true) {
                            @Override
                            public void handleResponse(String response) {
                                String successStr = "";
                                Log.d("Respose---->>>>", response);
                                try {
                                    JSONObject mainObj = new JSONObject(response);
                                    if (mainObj.getString("success").equals("1")) {
                                        AppGlobal.DistributerID = mainObj.getString("id");
                                        AppGlobal.Email = userRegisterParceble.getEmail().trim();
                                        AppGlobal.Name = userRegisterParceble.getFirst_name().trim();
                                        AppGlobal.MobileNumber = userRegisterParceble.getMobileNumber().trim();
                                        AppGlobal.PayToAmt = edtPrice.getText().toString().trim();
                                        AppGlobal.VechileNo = edtVechileNumber.getText().toString().trim();
                                        AppGlobal.ClassName = className.trim();
                                        AppGlobal.ClassID = class_id.trim();
                                        AppGlobal.fromWhere = "Register";
                                        UtilityMethod.showAlertBoxwithIntent(mContext, mainObj.getString("message"), PayUActivity.class);
                                        // UtilityMethod.goNextClass(mContext, AcharyaDeshBoardActivity.class);
                                    } else {
                                        UtilityMethod.showAlertBox(mContext, mainObj.getString("message"));
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        };
                        MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
                        entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
                        entityBuilder.addTextBody("first_name", userRegisterParceble.getFirst_name());
                        entityBuilder.addTextBody("last_name", userRegisterParceble.getLast_name());
                        entityBuilder.addTextBody("gender", userRegisterParceble.getGender());
                        entityBuilder.addTextBody("dob", userRegisterParceble.getDob());
                        entityBuilder.addTextBody("email", userRegisterParceble.getEmail());
                        entityBuilder.addTextBody("mobilenumber", userRegisterParceble.getMobileNumber());
                        entityBuilder.addTextBody("state_id", userRegisterParceble.getState());
                        entityBuilder.addTextBody("city_id", userRegisterParceble.getCity());
                        entityBuilder.addTextBody("pincode", userRegisterParceble.getPincode());
                        entityBuilder.addTextBody("class_id", class_id);
                        entityBuilder.addTextBody("class_price", edtPrice.getText().toString());
                        entityBuilder.addTextBody("veh_no", edtVechileNumber.getText().toString());
                        entityBuilder.addTextBody("aadhar", userRegisterParceble.getAadhar());
                        entityBuilder.addTextBody("address", userRegisterParceble.getAddress());
                        //  Log.d("imagePath>>>>", "" + new File(imagePath).getName());
                        File adharFile = saveBitmapToFile(new File(imgPathAadhar.toString()));
                        Log.d(TAG, "registerDistributer: " + Integer.parseInt(String.valueOf(adharFile.length() / 1024)));
                        File rcFrontFile = saveBitmapToFile(new File(imgPathRcFront.toString()));
                        Log.d(TAG, "registerDistributer: " + Integer.parseInt(String.valueOf(rcFrontFile.length() / 1024)));
                        File rcBackFile = saveBitmapToFile(new File(imgPathRcBack.toString()));
                        Log.d(TAG, "registerDistributer: " + Integer.parseInt(String.valueOf(rcBackFile.length() / 1024)));
                        entityBuilder.addBinaryBody("rc_front", rcFrontFile, ContentType.create("image/jpeg"), rcFrontFile.getName());
                        entityBuilder.addBinaryBody("rc_back", rcBackFile, ContentType.create("image/jpeg"), rcBackFile.getName());
                        entityBuilder.addBinaryBody("aadhar_pic", adharFile, ContentType.create("image/jpeg"), adharFile.getName());
                        HttpEntity entity = entityBuilder.build();
                        addCarCaller.addEntity(entity);
                        addCarCaller.execute(AppGlobal.RegisterUser);
                    }
                } else {
                    UtilityMethod.showAlertBox(mContext, "You have not connected to internet. Please Connect to internet and try again!");

                }

                break;

        }

    }

    public File saveBitmapToFile(File file) {
        try {

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE = 75;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 70, outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }

    public void permissionAccess() {
        if (ActivityCompat.checkSelfPermission(RegisterDocumentUploadActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(RegisterDocumentUploadActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(RegisterDocumentUploadActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(RegisterDocumentUploadActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(RegisterDocumentUploadActivity.this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(RegisterDocumentUploadActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(RegisterDocumentUploadActivity.this);
                builder.setTitle("Need Storage and Camera Permission");
                builder.setMessage("This app needs storage and Camera permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(RegisterDocumentUploadActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (permissionStatus.getBoolean(Manifest.permission.WRITE_EXTERNAL_STORAGE, false) || permissionStatus.getBoolean(Manifest.permission.CAMERA, false) || permissionStatus.getBoolean(Manifest.permission.READ_EXTERNAL_STORAGE, false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(RegisterDocumentUploadActivity.this);
                builder.setTitle("Need Storage and Camera Permission");
                builder.setMessage("This app needs storage and Camera permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getBaseContext(), "Go to Permissions to Grant Storage and Camera", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(RegisterDocumentUploadActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
            }
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.WRITE_EXTERNAL_STORAGE, true);
            editor.putBoolean(Manifest.permission.CAMERA, true);
            editor.putBoolean(Manifest.permission.READ_EXTERNAL_STORAGE, true);
            editor.commit();
        } else {
            //You already have the permission, just go ahead.
            selectImage();
        }
    }

    private void selectImage() {

        final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Upload Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                        }
                        if (photoFile != null) {
                            Uri photoURI;
                            if (Build.VERSION.SDK_INT >= 24) {
                                photoURI = FileProvider.getUriForFile(RegisterDocumentUploadActivity.this,
                                        BuildConfig.APPLICATION_ID + ".provider", photoFile);
                            } else {
                                photoURI = Uri.fromFile(photoFile);
                            }
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                            startActivityForResult(takePictureIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                        }
                    }
                } else if (items[item].equals("Choose from Gallery")) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                        Intent i = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, GALLERY_INTENT_CALLED);
                    } else {
                        Intent i = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, GALLERY_INTENT_CALLED);
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        if (imageView.equals("imgRcBack")) {
            imgPathRcBack = image.getAbsolutePath();
            Log.e("Getpath", "imgRcBack>>>" + imgPathRcBack);
        } else if (imageView.equals("imgAadhar")) {
            imgPathAadhar = image.getAbsolutePath();

            Log.e("Getpath", "imgPathAadhar>>>" + imgPathAadhar);
        } else if (imageView.equals("imgRcFront")) {
            imgPathRcFront = image.getAbsolutePath();

            Log.e("Getpath", "imgPathRcFront>>>" + imgPathRcFront);
        }
        return image;

    }

    @SuppressLint("LongLogTag")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Activity activity = (Activity) mContext;
                if (imageView.equals("imgRcBack")) {
                    int targetW = imgRcBack.getWidth();
                    int targetH = imgRcBack.getHeight();
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(imgPathRcBack, bmOptions);
                    int photoW = bmOptions.outWidth;
                    int photoH = bmOptions.outHeight;
                    int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
                    bmOptions.inJustDecodeBounds = false;
                    bmOptions.inSampleSize = 4;
                    bmOptions.inPurgeable = true;
                    Bitmap bitmap = BitmapFactory.decodeFile(imgPathRcBack, bmOptions);
                    imgRcBack.setImageBitmap(bitmap);

                } else if (imageView.equals("imgAadhar")) {
                    int targetW = imgAadhar.getWidth();
                    int targetH = imgAadhar.getHeight();
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(imgPathAadhar, bmOptions);
                    int photoW = bmOptions.outWidth;
                    int photoH = bmOptions.outHeight;
                    int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
                    bmOptions.inJustDecodeBounds = false;
                    bmOptions.inSampleSize = 4;
                    bmOptions.inPurgeable = true;
                    Bitmap bitmap = BitmapFactory.decodeFile(imgPathAadhar, bmOptions);
                    imgAadhar.setImageBitmap(bitmap);
                } else if (imageView.equals("imgRcFront")) {
                    int targetW = imgRcFront.getWidth();
                    int targetH = imgRcFront.getHeight();
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(imgPathRcFront, bmOptions);
                    int photoW = bmOptions.outWidth;
                    int photoH = bmOptions.outHeight;
                    int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
                    bmOptions.inJustDecodeBounds = false;
                    bmOptions.inSampleSize = 4;
                    bmOptions.inPurgeable = true;
                    Bitmap bitmap = BitmapFactory.decodeFile(imgPathRcFront, bmOptions);
                    imgRcFront.setImageBitmap(bitmap);


                 /*   File frontFile = new File(imgPathRcFront);
                    long frontSizeInBytes = frontFile.length();
                    long frontSizeMB = frontSizeInBytes / (1024 * 1024);
                    Log.d("frontSizeMB>>>", "" + frontSizeMB);*/
                }
            }
        }
        if (requestCode == GALLERY_INTENT_CALLED && data != null) {
            Uri uri = data.getData();
            if (imageView.equals("imgRcBack")) {
                imgPathRcBack = RealPathUtil.getPath(mContext, uri);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 4;
                Bitmap bitmap = BitmapFactory.decodeFile(imgPathRcBack, options);
                imgRcBack.setImageBitmap(bitmap);

                Log.d("imagePathRegistrationC>>>", imgPathRcBack);
            } else if (imageView.equals("imgAadhar")) {
                imgPathAadhar = RealPathUtil.getPath(mContext, uri);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 4;
                Bitmap bitmap = BitmapFactory.decodeFile(imgPathAadhar, options);
                imgAadhar.setImageBitmap(bitmap);

                Log.d("imagePathPanCard>>>", imgPathAadhar);
            } else if (imageView.equals("imgRcFront")) {

                imgPathRcFront = RealPathUtil.getPath(mContext, uri);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 4;
                Bitmap bitmap = BitmapFactory.decodeFile(imgPathRcFront, options);
                imgRcFront.setImageBitmap(bitmap);
                Log.d("imagePathAddress>>>", imgPathRcFront);

            }
        }
    }


    @SuppressLint("LongLogTag")
    public void setClassesList(final ArrayList<ClassesPojo> mClassesList) {

        Log.d(TAG, "setStateList: " + mClassesList.size());
        ArrayList<String> mNameList = new ArrayList<>();
        mNameList.add("Select Classes");
        for (int i = 0; i < mClassesList.size(); i++) {
            mNameList.add(mClassesList.get(i).class_name);
        }
        ArrayAdapter<String> mAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, mNameList);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinClass.setAdapter(mAdapter);

        spinClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String txt = adapterView.getItemAtPosition(position).toString();
                Log.d(TAG, "txt: " + txt);
                if (!txt.equals("Select Classes")) {
                    className = mClassesList.get(position - 1).class_name;
                    class_id = mClassesList.get(position - 1).class_id;
                    Log.d(TAG, "class_id: " + class_id);
                    edtPrice.setText(mClassesList.get(position - 1).class_price);

                } else {
                    edtPrice.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
