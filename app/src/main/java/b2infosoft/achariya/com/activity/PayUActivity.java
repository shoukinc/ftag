package b2infosoft.achariya.com.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.payumoney.core.PayUmoneyConfig;
import com.payumoney.core.PayUmoneyConstants;
import com.payumoney.core.PayUmoneySdkInitializer;
import com.payumoney.core.entity.TransactionResponse;
import com.payumoney.sdkui.ui.utils.PayUmoneyFlowManager;
import com.payumoney.sdkui.ui.utils.ResultModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;

import b2infosoft.achariya.com.R;
import b2infosoft.achariya.com.global.AppGlobal;
import b2infosoft.achariya.com.useful.UtilityMethod;

import static com.payumoney.core.SdkSession.hashCal;

public class PayUActivity extends AppCompatActivity {
    private static final String TAG = "FragmentPayU";
    View mainView;
    Context mContext;
    TextView tvTitle, tvMoney;
    String payAmt = "";
    String EventTitle = "product";
    String EventTitleHashConvert = "";
    String From = "";
    EditText etName, etMobile, etEmail, etAddress, etQuery, etAmount;
    Button btnSubmit;
    String txnId = "0nf7";
    private ProgressDialog nDialog;

    String MarchantKey = "U1Xz2cIX";
    String MarchantId = "6109518";
    String SaltKey = "1ZGxlcrvTx";

   /* String MarchantKey = "eY1ZL79m";
    String MarchantId = "5868512";
    String SaltKey = "vEPj34Jh4x";*/

    Toolbar toolbar;
    TextView toolbar_title, tvAmtTxt, tvRsTxt;
    private PayUmoneySdkInitializer.PaymentParam mPaymentParams;
    LinearLayout layoutAddress;
    String firstName = "", status = "", amount = "", txnid = "", hash = "", key = "", productinfo = "", email = "", additionalCharges = "";

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acharya_activity_pay_u);

        mContext = PayUActivity.this;
        tvTitle = findViewById(R.id.tvTitle);
        tvMoney = findViewById(R.id.tvMoney);
        etName = findViewById(R.id.etName);
        etMobile = findViewById(R.id.etMobile);
        etEmail = findViewById(R.id.etEmail);
        etAddress = findViewById(R.id.etAddress);
        etAmount = findViewById(R.id.etAmount);
        etQuery = findViewById(R.id.etQuery);
        layoutAddress = findViewById(R.id.layoutAddress);
        btnSubmit = findViewById(R.id.btnSubmit);
        tvMoney.setText(AppGlobal.PayToAmt);
        etEmail.setText(AppGlobal.Email);
        etMobile.setText(AppGlobal.MobileNumber);
        etAddress.setText(AppGlobal.VechileNo);
        etName.setText(AppGlobal.Name);
        tvTitle.setText(AppGlobal.ClassName);
       // etAmount.setText(AppGlobal.PayToAmt);
        etAmount.setText("5000");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        tvAmtTxt = findViewById(R.id.tvAmtTxt);
        tvRsTxt = findViewById(R.id.tvRsTxt);
        etAddress.setText(AppGlobal.ClassName);
        toolbar_title.setText("Payment");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        if (AppGlobal.VechileNo.equals("")) {
            layoutAddress.setVisibility(View.GONE);
        } else {
            layoutAddress.setVisibility(View.GONE);
            layoutAddress.setVisibility(View.VISIBLE);
        }
        if (!AppGlobal.fromWhere.equalsIgnoreCase("Distributorship")) {
            etAmount.setEnabled(true);
            etAmount.setFocusable(true);
            etAmount.setFocusableInTouchMode(true);
            etAmount.setClickable(true);
        }

        toolbar_title.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        btnSubmit.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvMoney.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        etEmail.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        etMobile.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        etAddress.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        etName.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        tvRsTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        tvAmtTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        etAmount.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));

        //EventTitle = AppGlobal.ClassID.trim();
        EventTitle = "product";
        EventTitleHashConvert = EventTitle;
        EventTitleHashConvert = EventTitleHashConvert.replace(",", "%2c");
        EventTitleHashConvert = EventTitleHashConvert.replace("&", "%2c");
        EventTitleHashConvert = EventTitleHashConvert.replace(",", "%2c");

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (Validations.isEditTextFilled(etName, "Enter Valid Name") &&
                        Validations.isValidMobile(etMobile, "Enter Valid Mobile Number") &&
                        Validations.isEditTextFilled(etAddress, "Enter Valid Address")) {
*/
                AppGlobal.PayToAmt = etAmount.getText().toString().trim();
                double amount = 0;
                try {
                    amount = Double.parseDouble(AppGlobal.PayToAmt.trim());
                    // amount = Double.parseDouble("1");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //Distributer
                completePayment(amount,
                        etName.getText().toString().trim().toLowerCase(),
                        etMobile.getText().toString().trim().toLowerCase(),
                        etEmail.getText().toString().trim().toLowerCase(),
                        etAddress.getText().toString().trim().toLowerCase(),
                        EventTitleHashConvert
                );
                //}
            }
        });
    }

    private void completePayment(double Charge, String Name, String Mobile, String Email, String Address, String paytitle) {

        PayUmoneyConfig payUmoneyConfig = PayUmoneyConfig.getInstance();
        //Use this to set your custom text on result screen button
        payUmoneyConfig.setDoneButtonText("OK");
        //Use this to set your custom title for the activity
        payUmoneyConfig.setPayUmoneyActivityTitle(mContext.getResources().getString(R.string.app_name));
        PayUmoneySdkInitializer.PaymentParam.Builder builder = new PayUmoneySdkInitializer.PaymentParam.Builder();

        long time = System.currentTimeMillis();
        txnId = txnId + time;

        Log.d(TAG, "txnId: " + txnId.trim());
        Log.d(TAG, "Charge: " + Charge);
        Log.d(TAG, "Mobile: " + Mobile.trim());
        Log.d(TAG, "paytitle: " + paytitle.trim());
        Log.d(TAG, "Name: " + Name.trim());
        Log.d(TAG, "MarchantKey: " + MarchantKey);
        Log.d(TAG, "MarchantId: " + MarchantId);

        builder.setAmount(Charge)            // Payment amount
                .setTxnId(txnId.trim())             // Transaction ID
                .setPhone(Mobile.trim())            // User Phone number
                .setProductName(paytitle.trim())    // Product Name or description
                .setFirstName(Name.trim())          // User First name
                .setEmail(Email.trim())             // User Email ID
                .setsUrl("https://www.payumoney.com/mobileapp/payumoney/success.php")           // Success URL (surl)
                .setfUrl("https://www.payumoney.com/mobileapp/payumoney/failure.php")           //Failure URL (furl)
                .setUdf1("")
                .setUdf2("")
                .setUdf3("")
                .setUdf4("")
                .setUdf5("")
                .setUdf6("")
                .setUdf7("")
                .setUdf8("")
                .setUdf9("")
                .setUdf10("")
                .setIsDebug(false)
                .setKey(MarchantKey)
                .setMerchantId(MarchantId);

        try {
            mPaymentParams = builder.build();
            //  generateHashFromServer(mPaymentParams);
            mPaymentParams = calculateServerSideHashAndInitiatePayment1(mPaymentParams);
            PayUmoneySdkInitializer.startPaymentActivityForResult((Activity) mContext, mPaymentParams);
            PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams, (Activity) mContext, R.style.AppTheme_default, false);

        } catch (Exception e) {
            // some exception occurred
            Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();

        }

    }

    private PayUmoneySdkInitializer.PaymentParam calculateServerSideHashAndInitiatePayment1(final PayUmoneySdkInitializer.PaymentParam paymentParam) {

        StringBuilder stringBuilder = new StringBuilder();
        HashMap<String, String> params = paymentParam.getParams();
        stringBuilder.append(params.get(PayUmoneyConstants.KEY) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.TXNID) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.AMOUNT) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.PRODUCT_INFO) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.FIRSTNAME) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.EMAIL) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF1) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF2) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF3) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF4) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF5) + "||||||");
        Log.d(TAG, "AllDataBefore: " + stringBuilder);
        stringBuilder.append(SaltKey);
        String hash = hashCal(stringBuilder.toString());
        paymentParam.setMerchantHash(hash);
        Log.d(TAG, "AllDataAfter: " + hash);
        return paymentParam;
    }


    public void generateHashFromServer(PayUmoneySdkInitializer.PaymentParam paymentParam) {
        //nextButton.setEnabled(false); // lets not allow the user to click the button again and again.

        HashMap<String, String> params = paymentParam.getParams();

        // lets create the post params
        StringBuffer postParamsBuffer = new StringBuffer();
        postParamsBuffer.append(concatParams(PayUmoneyConstants.KEY, params.get(PayUmoneyConstants.KEY)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.AMOUNT, params.get(PayUmoneyConstants.AMOUNT)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.TXNID, params.get(PayUmoneyConstants.TXNID)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.EMAIL, params.get(PayUmoneyConstants.EMAIL)));
        postParamsBuffer.append(concatParams("productinfo", params.get(PayUmoneyConstants.PRODUCT_INFO)));
        postParamsBuffer.append(concatParams("firstname", params.get(PayUmoneyConstants.FIRSTNAME)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.UDF1, params.get(PayUmoneyConstants.UDF1)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.UDF2, params.get(PayUmoneyConstants.UDF2)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.UDF3, params.get(PayUmoneyConstants.UDF3)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.UDF4, params.get(PayUmoneyConstants.UDF4)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.UDF5, params.get(PayUmoneyConstants.UDF5)));

        String postParams = postParamsBuffer.charAt(postParamsBuffer.length() - 1) == '&' ? postParamsBuffer.substring(0, postParamsBuffer.length() - 1).toString() : postParamsBuffer.toString();
        // lets make an api call
        GetHashesFromServerTask getHashesFromServerTask = new GetHashesFromServerTask();
        getHashesFromServerTask.execute(postParams);
    }

    protected String concatParams(String key, String value) {
        return key + "=" + value + "&";
    }

    private class GetHashesFromServerTask extends AsyncTask<String, String, String> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage("Please wait...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... postParams) {

            String merchantHash = "";
            try {
                //TODO Below url is just for testing purpose, merchant needs to replace this with their server side hash generation url
                URL url = new URL("https://payu.herokuapp.com/get_hash");
                String postParam = postParams[0];
                byte[] postParamsByte = postParam.getBytes("UTF-8");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postParamsByte);

                InputStream responseInputStream = conn.getInputStream();
                StringBuffer responseStringBuffer = new StringBuffer();
                byte[] byteContainer = new byte[1024];
                for (int i; (i = responseInputStream.read(byteContainer)) != -1; ) {
                    responseStringBuffer.append(new String(byteContainer, 0, i));
                }

                JSONObject response = new JSONObject(responseStringBuffer.toString());
                Iterator<String> payuHashIterator = response.keys();
                while (payuHashIterator.hasNext()) {
                    String key = payuHashIterator.next();
                    switch (key) {
                        /**
                         * This hash is mandatory and needs to be generated from merchant's server side
                         *
                         */
                        case "payment_hash":
                            merchantHash = response.getString(key);
                            break;
                        default:
                            break;
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return merchantHash;
        }

        @Override
        protected void onPostExecute(String merchantHash) {
            super.onPostExecute(merchantHash);
            progressDialog.dismiss();
            if (merchantHash.isEmpty() || merchantHash.equals("")) {
                Toast.makeText(mContext, "Could not generate hash", Toast.LENGTH_SHORT).show();
            } else {
                mPaymentParams.setMerchantHash(merchantHash);
                PayUmoneySdkInitializer.startPaymentActivityForResult((Activity) mContext, mPaymentParams);
                //PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams, mContext, AppPreference.selectedTheme, true);
                PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams, (Activity) mContext, R.style.AppTheme_default, false);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("MainActivity", "data" + data);
        Log.d("MainActivity", "request code " + requestCode + " resultcode " + resultCode);
        if (requestCode == PayUmoneyFlowManager.REQUEST_CODE_PAYMENT && resultCode == RESULT_OK && data != null) {
            TransactionResponse transactionResponse = data.getParcelableExtra(PayUmoneyFlowManager.INTENT_EXTRA_TRANSACTION_RESPONSE);
            ResultModel resultModel = data.getParcelableExtra(PayUmoneyFlowManager.ARG_RESULT);
            if (transactionResponse != null && transactionResponse.getPayuResponse() != null) {// Response from Payumoney
                String payuResponse = transactionResponse.getPayuResponse();
// Response from SURl and FURL

                String merchantResponse = transactionResponse.getTransactionDetails();
                Log.d(TAG, "payuResponse: " + payuResponse);
                Log.d(TAG, "merchantResponse: " + merchantResponse);
                try {
                    JSONObject jsonObject1 = new JSONObject(payuResponse);
                    JSONObject resultObj = jsonObject1.getJSONObject("result");
                    firstName = resultObj.getString("firstname");
                    status = resultObj.getString("status");
                    amount = resultObj.getString("amount");
                    txnid = resultObj.getString("txnid");
                    hash = resultObj.getString("hash");
                    key = resultObj.getString("key");
                    productinfo = resultObj.getString("productinfo");
                    email = resultObj.getString("email");
                    additionalCharges = resultObj.getString("additionalCharges");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (transactionResponse.getTransactionStatus().equals(TransactionResponse.TransactionStatus.SUCCESSFUL)) {
                    //Success Transaction

                    sendTransStatus("success");
                   /* } else {
                        UtilityMethod.showAlertBoxwithIntent(mContext, "Transaction Successfully Completed", AcharyaDeshBoardActivity.class);
                    }*/
                } else {
                    //Failure Transaction
                    //  if (AppGlobal.fromWhere.equals("Distributorship")) {
                    sendTransStatus("failure");
                   /* } else {
                        UtilityMethod.showAlertBox(mContext, "Transaction Not Successfully Completed");
                    }*/
                    // Response from SURl and FURL
                    /* new AlertDialog.Builder(this)
                        .setCancelable(false)
                        .setMessage("Payu's Data : " + payuResponse + "\n\n\n Merchant's Data: " + merchantResponse)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }).show();*/

                }
            } else if (resultModel != null && resultModel.getError() != null) {
                Log.d(TAG, "Error response : " + resultModel.getError().getTransactionResponse());
            } else {
                Log.d(TAG, "Both objects are null!");
            }
        }
    }

    @Override
    public void onBackPressed() {
        UtilityMethod.goNextClass(mContext, AcharyaDeshBoardActivity.class);
    }

    private void sendTransStatus(String status) {
        Log.d(TAG, "sendTransStatus: " + status);

        Intent intent = new Intent(mContext, SuccessFaildPaymentActivity.class);
        intent.putExtra("txnid", "" + txnid);
        intent.putExtra("status", this.status);
        intent.putExtra("firstname", firstName);
        intent.putExtra("amount", amount);
        intent.putExtra("hash", hash);
        intent.putExtra("key", key);
        intent.putExtra("productinfo", productinfo);
        intent.putExtra("email", email);
        intent.putExtra("additionalCharges", additionalCharges);
        intent.putExtra("dis_id", AppGlobal.DistributerID);
        intent.putExtra("from", AppGlobal.fromWhere);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


    }
}
