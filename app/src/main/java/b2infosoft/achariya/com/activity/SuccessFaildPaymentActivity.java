package b2infosoft.achariya.com.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import b2infosoft.achariya.com.R;
import b2infosoft.achariya.com.global.AppGlobal;
import b2infosoft.achariya.com.useful.UtilityMethod;
import b2infosoft.achariya.com.webservice.WebServiceCaller;

public class SuccessFaildPaymentActivity extends AppCompatActivity {


    Button btnDashboard;
    Toolbar toolbar;
    private static final String TAG = "SuccessFaildPaymentActi";
    TextView toolbar_title, tvTransType, tvDetails;
    ImageView imgTransType;
    String firstName = "", status = "", amount = "", txnid = "", hash = "", key = "", productinfo = "", email = "", additionalCharges = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acharya_activity_success_faild_payment);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Transaction Statement");
        toolbar_title.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        btnDashboard = findViewById(R.id.btnDashboard);
        tvTransType = findViewById(R.id.tvTransType);
        tvDetails = findViewById(R.id.tvDetails);

        btnDashboard.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvTransType.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvDetails.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        imgTransType = findViewById(R.id.imgTransType);
        Intent intent = getIntent();
        if (intent != null) {

            firstName = intent.getStringExtra("firstname");
            status = intent.getStringExtra("status");
            amount = intent.getStringExtra("amount");
            txnid = intent.getStringExtra("txnid");
            hash = intent.getStringExtra("hash");
            key = intent.getStringExtra("key");
            productinfo = intent.getStringExtra("productinfo");
            email = intent.getStringExtra("email");
            additionalCharges = intent.getStringExtra("additionalCharges");
            uploadData();
        }

        btnDashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UtilityMethod.goNextClass(SuccessFaildPaymentActivity.this, AcharyaDeshBoardActivity.class);
            }
        });
    }

    private void uploadData() {
        WebServiceCaller webServiceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, SuccessFaildPaymentActivity.this, "Please Wait...", true) {
            @Override
            public void handleResponse(String response) {
                Log.d(TAG, "handleResponse: " + response);
                try {
                    JSONObject mainObj = new JSONObject(response);
                    if (mainObj.getString("success").equals("1")) {
                        tvTransType.setText("PAYMENT SUCCESSFULL");
                        tvDetails.setText("You have send Rs " + amount + " successfully.");
                    } else {
                        tvTransType.setText("PAYMENT FAILED");
                        tvDetails.setText("Your Transaction Failed.");
                    }
                    // UtilityMethod.showAlertBoxwithIntent(mContext, mainObj.getString("message"), SuccessFaildPaymentActivity.class);
                    // UtilityMethod.goNextClass(mContext, AcharyaDeshBoardActivity.class);
                /* else{
                        UtilityMethod.showAlertBox(SuccessFaildPaymentActivity.this, mainObj.getString("message"));
                    }*/
                } catch (
                        JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        webServiceCaller.addNameValuePair("txnid", "" + txnid);
        webServiceCaller.addNameValuePair("status", this.status);
        webServiceCaller.addNameValuePair("firstname", firstName);
        webServiceCaller.addNameValuePair("amount", amount);
        webServiceCaller.addNameValuePair("hash", hash);
        webServiceCaller.addNameValuePair("key", key);
        webServiceCaller.addNameValuePair("productinfo", productinfo);
        webServiceCaller.addNameValuePair("email", email);
        webServiceCaller.addNameValuePair("additionalCharges", additionalCharges);
        webServiceCaller.addNameValuePair("dis_id", AppGlobal.DistributerID);
        if (status.equals("success")) {
            webServiceCaller.execute(AppGlobal.SuccessTransaction);
        } else {
            webServiceCaller.execute(AppGlobal.FailedTransaction);
        }
    }
}
