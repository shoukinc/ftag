package b2infosoft.achariya.com.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

import b2infosoft.achariya.com.R;
import b2infosoft.achariya.com.pojo.CityPojo;
import b2infosoft.achariya.com.pojo.StatePojo;
import b2infosoft.achariya.com.pojo.UserRegisterParceble;
import b2infosoft.achariya.com.useful.ConnectionDetector;
import b2infosoft.achariya.com.useful.UtilityMethod;

public class BasicInfoSecondActivity extends AppCompatActivity {
    Context mContext;
    Button btnNext;
    Toolbar toolbar;
    TextView toolbar_title;
    EditText edtEmail, edtdateofbirth, edtpincode;
    Spinner spinnerState, Spinner_city;
    private static final String TAG = "BasicInfoSecondActivity";
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String stateID = "", cityID = "";
    private int mYear, mMonth, mDay, mHour, mMinute;
    String formattedDate = "";
    ConnectionDetector connectionDetector;
    UserRegisterParceble userRegisterParceble;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acharya_activity_basic_info_second);
        mContext = BasicInfoSecondActivity.this;
        initView();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    private void initView() {
        connectionDetector = new ConnectionDetector(mContext);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Registration");
        toolbar_title.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        btnNext = (Button) findViewById(R.id.btnNext);
        btnNext.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtdateofbirth = (EditText) findViewById(R.id.edtdateofbirth);
        edtpincode = (EditText) findViewById(R.id.edtpincode);
        spinnerState = (Spinner) findViewById(R.id.spinnerState);
        Spinner_city = (Spinner) findViewById(R.id.Spinner_city);
        edtEmail.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        edtdateofbirth.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        edtpincode.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));

        Intent intent = getIntent();
        if (intent != null) {
            userRegisterParceble = (UserRegisterParceble) intent.getParcelableExtra("UserRegister");
            Log.d("userRegisterParceble", userRegisterParceble.getAadhar());
        }


        edtdateofbirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Activity activity = (Activity) mContext;
                UtilityMethod.hideKeyboardForFocusedView(activity);
                getDate();
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!edtEmail.getText().toString().equals("") && !edtdateofbirth.getText().toString().equals("")
                        && !edtpincode.getText().toString().equals("")) {

                    if (!edtEmail.getText().toString().matches(emailPattern)) {
                        UtilityMethod.showAlertBox(mContext, "Invalid Email Address.");
                    } else if (edtpincode.getText().toString().length() != 6) {
                        UtilityMethod.showAlertBox(mContext, "Invalid Pincode Number.");

                    } else if (stateID.equals("")) {
                        UtilityMethod.showAlertBox(mContext, "Please Select State.");
                    } else if (cityID.equals("")) {
                        UtilityMethod.showAlertBox(mContext, "Please Select City.");
                    } else {
                        Intent intent = new Intent(mContext, RegisterDocumentUploadActivity.class);
                        userRegisterParceble.setEmail(edtEmail.getText().toString());
                        userRegisterParceble.setDob(edtdateofbirth.getText().toString());
                        userRegisterParceble.setPincode(edtpincode.getText().toString());
                        userRegisterParceble.setState(stateID);
                        userRegisterParceble.setCity(cityID);
                        intent.putExtra("UserRegister", userRegisterParceble);
                        startActivity(intent);
                    }

                } else {
                    UtilityMethod.showAlertBox(mContext, "Please Enter All Fields.");
                }

            }
        });
        if (connectionDetector.isConnectingToInternet()) {
            StatePojo.getStateList(mContext, "BasicInfo");
        } else {
            UtilityMethod.showAlertBox(mContext, "You have not connected to internet. Please Connect to internet and try again!");
        }

    }

    public void getDate() {
        final Calendar c2 = Calendar.getInstance();
        mYear = c2.get(Calendar.YEAR);
        mMonth = c2.get(Calendar.MONTH);
        mDay = c2.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog2 = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String day = "";
                        day = checkDigit(dayOfMonth);
                        String newMonth = checkDigit((monthOfYear + 1));
                        formattedDate = day + "/" + newMonth + "/" + year;
                        edtdateofbirth.setText(formattedDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog2.show();
    }

    public String checkDigit(int number) {
        return number <= 9 ? "0" + number : String.valueOf(number);
    }

    public void setStateList(final ArrayList<StatePojo> mStateList) {
        Log.d(TAG, "setStateList: " + mStateList.size());
        ArrayList<String> mNameList = new ArrayList<>();
        final ArrayList<String> mCityNameList = new ArrayList<>();
        mCityNameList.add("Select City");
        mNameList.add("Select State");
        for (int i = 0; i < mStateList.size(); i++) {
            mNameList.add(mStateList.get(i).name);
        }
        ArrayAdapter<String> mAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, mNameList);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerState.setAdapter(mAdapter);

        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                String txt = adapterView.getItemAtPosition(position).toString();
                Log.d(TAG, "txt: " + txt);
                if (!txt.equals("Select State")) {
                    stateID = mStateList.get(position - 1).state_id;
                    Log.d(TAG, "stateID: " + stateID);
                    CityPojo.getCityList(mContext, stateID, "BasicInfo");
                } else {
                    stateID = "";
                    ArrayAdapter<String> mAdapter1 = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, mCityNameList);
                    mAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    Spinner_city.setAdapter(mAdapter1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void setCityList(final ArrayList<CityPojo> mCityList) {
        final ArrayList<String> mCityNameList = new ArrayList<>();
        mCityNameList.add("Select City");
        for (int i = 0; i < mCityList.size(); i++) {
            mCityNameList.add(mCityList.get(i).name);
        }
        ArrayAdapter<String> mAdapter1 = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, mCityNameList);
        mAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner_city.setAdapter(mAdapter1);
        Spinner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String txt = adapterView.getItemAtPosition(position).toString();
                Log.d(TAG, "txt: " + txt);
                if (!txt.equals("Select City")) {
                    cityID = mCityList.get(position - 1).city_id;
                    Log.d(TAG, "cityID: " + cityID);
                } else {
                    cityID = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
