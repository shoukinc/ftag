package b2infosoft.achariya.com.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import b2infosoft.achariya.com.R;
import b2infosoft.achariya.com.global.AppGlobal;
import b2infosoft.achariya.com.pojo.UserRegisterParceble;
import b2infosoft.achariya.com.useful.UtilityMethod;
import b2infosoft.achariya.com.webservice.WebServiceCaller;

public class RegisterBasicInfoActivity extends AppCompatActivity {

    Context mContext;
    Button btnNext;
    Toolbar toolbar;
    TextView toolbar_title;
    EditText edt_Mobileno, edt_FirstName, edtLastName, edtAddress, edtAdharNumber;
    Spinner spinnerGender;
    String[] gender = {"Select Gender", "Male", "Female"};
    private static final String TAG = "RegisterBasicInfoActivi";
    String str_gender = "";
    String value = "false";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acharya_activity_register_basic_info);

        mContext = RegisterBasicInfoActivity.this;
        initView();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Registration");
        toolbar_title.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        btnNext = (Button) findViewById(R.id.btnNext);
        btnNext.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        edt_Mobileno = (EditText) findViewById(R.id.edt_Mobileno);
        edt_FirstName = (EditText) findViewById(R.id.edt_FirstName);
        edtLastName = (EditText) findViewById(R.id.edtLastName);
        edtAddress = (EditText) findViewById(R.id.edtAddress);
        edtAdharNumber = (EditText) findViewById(R.id.edtAdharNumber);
        spinnerGender = (Spinner) findViewById(R.id.spinnerGender);
        edt_Mobileno.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        edtAdharNumber.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        edt_FirstName.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        edtLastName.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        edtAddress.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));

        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, gender);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spinnerGender.setAdapter(aa);
        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String txt = adapterView.getItemAtPosition(position).toString();
                Log.d(TAG, "onItemSelected: " + txt);
                if (!txt.equals("Select Gender")) {
                    str_gender = adapterView.getItemAtPosition(position).toString();
                } else {
                    str_gender = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        edt_Mobileno.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 10) {
                    final WebServiceCaller webServiceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait...", true) {
                        @Override
                        public void handleResponse(String response) {

                            Log.d("response>>>", response);
                            try {
                                JSONObject jsonObject1 = new JSONObject(response);
                                if (jsonObject1.getString("success").equals("true")) {
                                    UtilityMethod.showAlertBox(mContext, "Mobile Number Already Exist");
                                    value = "true";
                                } else {
                                    value = "false";
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    };
                    webServiceCaller.addNameValuePair("mobile_no", edt_Mobileno.getText().toString().trim());
                    webServiceCaller.addNameValuePair("type", "user");
                    webServiceCaller.execute(AppGlobal.CheckMobile);
                }
            }
        });


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!edt_Mobileno.getText().toString().trim().equals("") && !edt_FirstName.getText().toString().trim().equals("") &&
                        !edtAddress.getText().toString().trim().equals("") && !edtLastName.getText().toString().trim().equals("") &&
                        !edtAdharNumber.getText().toString().trim().equals("")
                        ) {

                    if (value.equals("true")) {
                        UtilityMethod.showAlertBox(mContext, "Mobile Number Already Exist.");
                    } else if (str_gender.equals("")) {
                        UtilityMethod.showAlertBox(mContext, "Please Select Gender.");
                    } else if (edt_Mobileno.getText().toString().trim().length() != 10) {
                        UtilityMethod.showAlertBox(mContext, "Invalid Mobile Number.");
                    } else if (edtAdharNumber.getText().toString().trim().length() != 12) {
                        UtilityMethod.showAlertBox(mContext, "Invalid Aadhar Number.");
                    } else {
                        Intent intent = new Intent(mContext, BasicInfoSecondActivity.class);
                        UserRegisterParceble userRegisterParceble = new UserRegisterParceble();
                        userRegisterParceble.setMobileNumber(edt_Mobileno.getText().toString());
                        userRegisterParceble.setFirst_name(edt_FirstName.getText().toString());
                        userRegisterParceble.setLast_name(edtLastName.getText().toString());
                        userRegisterParceble.setGender(str_gender);
                        userRegisterParceble.setAddress(edtAddress.getText().toString());
                        userRegisterParceble.setAadhar(edtAdharNumber.getText().toString());
                        intent.putExtra("UserRegister", userRegisterParceble);
                        startActivity(intent);
                    }
                } else {
                    UtilityMethod.showAlertBox(mContext, "Please Enter All Fields.");
                }
            }
        });
    }
}
