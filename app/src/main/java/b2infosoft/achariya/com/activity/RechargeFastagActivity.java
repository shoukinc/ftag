package b2infosoft.achariya.com.activity;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import b2infosoft.achariya.com.R;
import b2infosoft.achariya.com.global.AppGlobal;
import b2infosoft.achariya.com.pojo.ClassesPojo;
import b2infosoft.achariya.com.useful.ConnectionDetector;
import b2infosoft.achariya.com.useful.UtilityMethod;
import b2infosoft.achariya.com.webservice.WebServiceCaller;

public class RechargeFastagActivity extends AppCompatActivity {
    Context mContext;
    Toolbar toolbar;
    Button btnrechargeSubmit;
    TextView toolbar_title;
    ConnectionDetector connectionDetector;
    String Mobileno = "", name = "", vehicleno = "", Email = "", Adharno, class_id1 = "";
    String str_vehicleclass = "";
    public static final String TAG = "Responce";
    EditText edt_RechargeMobileno, edt_RechargeName, edt_RechargeEmail, edt_Recharge_Aadhar, edt_RechargeVehicle, edt_RechargeAmount;

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    Spinner spinnerVehicleclass;
    String className = "";
    String value = "false";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acharya_activity_recharge_fastag);

        mContext = RechargeFastagActivity.this;
        initView();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    private void initView() {
        connectionDetector = new ConnectionDetector(mContext);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Recharge");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        toolbar_title.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        spinnerVehicleclass = (Spinner) findViewById(R.id.spinnerVehicleclass);
        edt_RechargeMobileno = (EditText) findViewById(R.id.edt_RechargeMobileno);
        edt_RechargeName = (EditText) findViewById(R.id.edt_RechargeName);
        edt_RechargeEmail = (EditText) findViewById(R.id.edt_RechargeEmail);
        edt_Recharge_Aadhar = (EditText) findViewById(R.id.edt_Recharge_Aadhar);
        edt_RechargeVehicle = (EditText) findViewById(R.id.edt_RechargeVehicle);
        edt_RechargeAmount = (EditText) findViewById(R.id.edt_RechargeAmount);
        btnrechargeSubmit = (Button) findViewById(R.id.btnrechargeSubmit);
        btnrechargeSubmit.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        edt_RechargeAmount.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        edt_RechargeVehicle.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        edt_Recharge_Aadhar.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        edt_RechargeEmail.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        edt_RechargeMobileno.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        edt_RechargeName.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));

        if (connectionDetector.isConnectingToInternet()) {
            ClassesPojo.getClassesList(mContext, "RechargeFastagActivity");
        } else {
            UtilityMethod.showAlertBox(mContext, "You have not connected to internet. Please Connect to internet and try again!");
        }


        edt_RechargeMobileno.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 10) {
                    final WebServiceCaller webServiceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait...", true) {
                        @Override
                        public void handleResponse(String response) {

                            Log.d("response>>>", response);
                            try {
                                JSONObject jsonObject1 = new JSONObject(response);
                                if (jsonObject1.getString("success").equals("true")) {
                                    UtilityMethod.showAlertBox(mContext, "Mobile Number Already Exist");
                                    value = "true";
                                } else {
                                    value = "false";
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    };
                    webServiceCaller.addNameValuePair("mobile_no", edt_RechargeMobileno.getText().toString().trim());
                    webServiceCaller.addNameValuePair("type", "fasttag");
                    // webServiceCaller.execute(AppGlobal.CheckMobile);
                }
            }
        });

        btnrechargeSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!edt_RechargeAmount.getText().toString().trim().equals("") && !edt_RechargeMobileno.getText().toString().trim().equals("") && !edt_RechargeVehicle.getText().toString().trim().equals("") &&
                        !edt_RechargeName.getText().toString().trim().equals("") && !edt_Recharge_Aadhar.getText().toString().trim().equals("") &&
                        !edt_RechargeEmail.getText().toString().trim().equals("")
                        ) {

                    if (value.equals("true")) {
                        UtilityMethod.showAlertBox(mContext, "Mobile Number Already Exist.");
                    } else if (class_id1.equals("")) {
                        UtilityMethod.showAlertBox(mContext, "Please Select Vehicle Class.");
                    } else if (edt_RechargeMobileno.getText().toString().trim().length() != 10) {
                        UtilityMethod.showAlertBox(mContext, "Invalid Mobile Number.");
                    } else if (edt_Recharge_Aadhar.getText().toString().trim().length() != 12) {
                        UtilityMethod.showAlertBox(mContext, "Invalid Aadhar Number.");
                    } else if (!edt_RechargeEmail.getText().toString().matches(emailPattern)) {
                        UtilityMethod.showAlertBox(mContext, "Invalid Email Address.");
                    } else {

                        registerFastag();
                    }
                } else {
                    UtilityMethod.showAlertBox(mContext, "Please Enter All Fields.");
                }
            }
        });
    }

    private void registerFastag() {
        WebServiceCaller webServiceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait...", true) {
            @Override
            public void handleResponse(String response) {

                Log.d("response>>>", response);
                try {
                    JSONObject mainObj = new JSONObject(response);
                    if (mainObj.getString("success").equals("1")) {
                        AppGlobal.DistributerID = mainObj.getString("id");
                        AppGlobal.Email = edt_RechargeEmail.getText().toString().trim();
                        AppGlobal.Name = edt_RechargeName.getText().toString().trim();
                        AppGlobal.MobileNumber = edt_RechargeMobileno.getText().toString().trim();
                        AppGlobal.PayToAmt = edt_RechargeAmount.getText().toString().trim();
                        AppGlobal.VechileNo = edt_RechargeVehicle.getText().toString().trim();
                        AppGlobal.ClassName = className.trim();
                        AppGlobal.ClassID = class_id1.trim();
                        AppGlobal.fromWhere = "RechargeFastTag";
                        UtilityMethod.showAlertBoxwithIntent(mContext, mainObj.getString("message"), PayUActivity.class);
                        // UtilityMethod.goNextClass(mContext, AcharyaDeshBoardActivity.class);
                    } else {
                        UtilityMethod.showAlertBox(mContext, mainObj.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        webServiceCaller.addNameValuePair("fr_name", edt_RechargeName.getText().toString());
        webServiceCaller.addNameValuePair("fr_email", edt_RechargeEmail.getText().toString());
        webServiceCaller.addNameValuePair("fr_mobileno", edt_RechargeMobileno.getText().toString());
        webServiceCaller.addNameValuePair("fr_vehno", edt_RechargeVehicle.getText().toString());
        webServiceCaller.addNameValuePair("fr_aadharno", edt_Recharge_Aadhar.getText().toString());
        webServiceCaller.addNameValuePair("class_id", class_id1);
        webServiceCaller.addNameValuePair("fr_amount", edt_RechargeAmount.getText().toString());
        webServiceCaller.execute(AppGlobal.RegisterFastag);
    }

    public void setClassesList(final ArrayList<ClassesPojo> mClassesList) {

        Log.d(TAG, "setStateList: " + mClassesList.size());
        ArrayList<String> mNameList = new ArrayList<>();
        mNameList.add("Select Classes");
        for (int i = 0; i < mClassesList.size(); i++) {
            mNameList.add(mClassesList.get(i).class_name);
        }
        ArrayAdapter<String> mAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, mNameList);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerVehicleclass.setAdapter(mAdapter);

        spinnerVehicleclass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String txt = adapterView.getItemAtPosition(position).toString();
                Log.d(TAG, "txt: " + txt);
                if (!txt.equals("Select Classes")) {
                    className = mClassesList.get(position - 1).class_name;
                    class_id1 = mClassesList.get(position - 1).class_id;
                    Log.d(TAG, "class_id: " + class_id1);
                    edt_RechargeAmount.setText(mClassesList.get(position - 1).class_price);

                } else {
                    edt_RechargeAmount.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }


}
