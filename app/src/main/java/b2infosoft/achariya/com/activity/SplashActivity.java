package b2infosoft.achariya.com.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import java.util.ArrayList;

import b2infosoft.achariya.com.R;
import b2infosoft.achariya.com.addley.activity.LoginActivity;
import b2infosoft.achariya.com.useful.ConnectivityReceiver;
import b2infosoft.achariya.com.useful.ForceUpdateChecker;
import b2infosoft.achariya.com.useful.UtilityMethod;

public class SplashActivity extends AppCompatActivity implements ForceUpdateChecker.OnUpdateNeededListener, ConnectivityReceiver.ConnectivityReceiverListener {

    ImageView imgLogo;
    boolean isConnectedToInternet = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acharya_activity_splash);
        imgLogo = (ImageView) findViewById(R.id.imgLogo);
        isConnectedToInternet = ConnectivityReceiver.isConnected();
        zoom_in();
    }

    @Override
    public void onUpdateNeeded(final String updateUrl) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("New Update available")
                .setMessage("Please, update app to new version and continue.")
                .setCancelable(false)
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore(updateUrl);
                            }
                        })/*.setNegativeButton("No, thanks",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })*/.create();
        dialog.show();


    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void zoom_in() {
        Animation animation1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
        imgLogo.startAnimation(animation1);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isConnectedToInternet) {
                    ForceUpdateChecker.with(SplashActivity.this).onUpdateNeeded(SplashActivity.this).check();
                } else {
                    UtilityMethod.showAlertBox(SplashActivity.this, "You have no Internet Connection, Please Connect to the Internet.");
                }
            }
        }, 4000);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        if (isConnected) {
            ForceUpdateChecker.with(SplashActivity.this).onUpdateNeeded(SplashActivity.this).check();
        }

    }

}




