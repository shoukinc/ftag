package b2infosoft.achariya.com.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import b2infosoft.achariya.com.R;

public class LoginviaActivity extends AppCompatActivity implements View.OnClickListener {

    Context mContext;
    Button btnBuyFasttag, btnDistributorship, btnRechargeFasttag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acharya_activity_loginvia);

        mContext = LoginviaActivity.this;

        initView();

    }

    private void initView() {

        btnBuyFasttag = (Button) findViewById(R.id.btnBuyFasttag);
        btnDistributorship = (Button) findViewById(R.id.btnDistributorship);
        btnRechargeFasttag = (Button) findViewById(R.id.btnRechargeFasttag);

        btnBuyFasttag.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        btnDistributorship.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        btnRechargeFasttag.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));


        btnBuyFasttag.setOnClickListener(this);

        btnDistributorship.setOnClickListener(this);
        btnRechargeFasttag.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnBuyFasttag:
                startActivity(new Intent(mContext, RegisterBasicInfoActivity.class));
                break;
            case R.id.btnDistributorship:
                startActivity(new Intent(mContext, DistributorshipActivity.class));
                break;
            case R.id.btnRechargeFasttag:
                startActivity(new Intent(mContext, RechargeFastagActivity.class));

                break;
        }
    }
}
