package b2infosoft.achariya.com.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import b2infosoft.achariya.com.BuildConfig;
import b2infosoft.achariya.com.R;
import b2infosoft.achariya.com.global.AppGlobal;
import b2infosoft.achariya.com.pojo.CityPojo;
import b2infosoft.achariya.com.pojo.StatePojo;
import b2infosoft.achariya.com.useful.ConnectionDetector;
import b2infosoft.achariya.com.useful.RealPathUtil;
import b2infosoft.achariya.com.useful.UtilityMethod;
import b2infosoft.achariya.com.webservice.WebServiceCaller;

public class DistributorshipActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    TextView toolbar_title;
    EditText edt_Mobileno, edt_Name, edtEmail, edtAdharNumber;
    Context mContext;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    Spinner spinnerState, Spinner_city;
    ImageView imgAadhar;
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private SharedPreferences permissionStatus;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 101;
    private static final int GALLERY_INTENT_CALLED = 201;
    private String imageView = "", stateID = "", cityID = "";
    private boolean sentToSettings = false;
    String imgPathAadhar = "";
    TextView tvAadharTxt;
    ConnectionDetector connectionDetector;
    private static final String TAG = "DistributorshipActivity";
    Button btnSubmit;
    String value = "false";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acharya_activity_distributorship);
        mContext = DistributorshipActivity.this;
        initView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    private void initView() {

        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Distributor");
        toolbar_title.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        connectionDetector = new ConnectionDetector(mContext);
        imgAadhar = (ImageView) findViewById(R.id.imgAadhar);
        edt_Mobileno = (EditText) findViewById(R.id.edt_Mobileno);
        edtAdharNumber = (EditText) findViewById(R.id.edtAdharNumber);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edt_Name = (EditText) findViewById(R.id.edt_Name);
        tvAadharTxt = (TextView) findViewById(R.id.tvAadharTxt);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        edt_Mobileno.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        edtAdharNumber.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        edt_Name.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvAadharTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        edtEmail.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        btnSubmit.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        spinnerState = (Spinner) findViewById(R.id.spinnerState);
        Spinner_city = (Spinner) findViewById(R.id.Spinner_city);
        btnSubmit.setOnClickListener(this);
        imgAadhar.setOnClickListener(this);
        if (connectionDetector.isConnectingToInternet()) {
            StatePojo.getStateList(mContext, "Distributer");
        } else {
            UtilityMethod.showAlertBox(mContext, "You have not connected to internet. Please Connect to internet and try again!");
        }

        edt_Mobileno.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 10) {
                    final WebServiceCaller webServiceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait...", true) {
                        @Override
                        public void handleResponse(String response) {

                            Log.d("response>>>", response);
                            try {
                                JSONObject jsonObject1 = new JSONObject(response);
                                if (jsonObject1.getString("success").equals("true")) {
                                    UtilityMethod.showAlertBox(mContext, "Mobile Number Already Exist");
                                    value = "false";
                                } else {
                                    value = "false";
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    };
                    webServiceCaller.addNameValuePair("mobile_no", edt_Mobileno.getText().toString().trim());
                    webServiceCaller.addNameValuePair("type", "distributer");
                   // webServiceCaller.execute(AppGlobal.CheckMobile);
                }
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.imgAadhar:
                permissionAccess();
                break;
            case R.id.btnSubmit:
                if (connectionDetector.isConnectingToInternet()) {

                    if (!edt_Mobileno.getText().toString().trim().equals("") && !edt_Name.getText().toString().trim().equals("")
                            && !edtEmail.getText().toString().trim().equals("")) {
                        if (value.equals("true")) {
                            UtilityMethod.showAlertBox(mContext, "Mobile Number Already Exist.");
                        } else if (stateID.equals("")) {
                            UtilityMethod.showAlertBox(mContext, "Please Select State");
                        } else if (cityID.equals("")) {
                            UtilityMethod.showAlertBox(mContext, "Please Select City");
                        } else if (edtAdharNumber.getText().toString().length() != 12) {
                            UtilityMethod.showAlertBox(mContext, "Invalid Aadhar Number");
                        } else if (imgPathAadhar.equals("")) {
                            UtilityMethod.showAlertBox(mContext, "Please Select Aadhar Image");
                        } else if (!edtEmail.getText().toString().matches(emailPattern)) {
                            UtilityMethod.showAlertBox(mContext, "Invalid Email Address.");
                        } else {
                            registerDistributer();
                        }
                    } else {
                        UtilityMethod.showAlertBox(mContext, "Please Fill All Fields.");
                    }
                } else {
                    UtilityMethod.showAlertBox(mContext, "Mobile Number Already Exist.");
                }
                break;
        }
    }

    private void registerDistributer() {
        WebServiceCaller addCarCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait...", true) {
            @Override
            public void handleResponse(String response) {
                Log.d("Respose---->>>>", response);
                try {
                    JSONObject mainObj = new JSONObject(response);
                    if (mainObj.getString("success").equals("1")) {
                        AppGlobal.Email = edtEmail.getText().toString().trim();
                        AppGlobal.Name = edt_Name.getText().toString().trim();
                        AppGlobal.MobileNumber = edt_Mobileno.getText().toString().trim();
                        // AppGlobal.PayToAmt = e;
                        AppGlobal.VechileNo = "";
                        AppGlobal.ClassName = "Distributorship".trim();
                        AppGlobal.ClassID = "Distributorship".trim();
                        AppGlobal.DistributerID = mainObj.getString("dis_id");
                        AppGlobal.Otp = mainObj.getString("otp");
                        //UtilityMethod.showAlertBoxwithIntent(mContext, mainObj.getString("message"), OtpActivity.class);
                        // UtilityMethod.goNextClass(mContext, AcharyaDeshBoardActivity.class);
                        AppGlobal.fromWhere = "Distributorship";
                        showAlertBoxwithIntent(mContext, mainObj.getString("message"), OtpActivity.class);
                    } else {
                        UtilityMethod.showAlertBox(mContext, mainObj.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
        entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        entityBuilder.addTextBody("dis_name", edt_Name.getText().toString());
        entityBuilder.addTextBody("dis_email", edtEmail.getText().toString());
        entityBuilder.addTextBody("dis_number", edt_Mobileno.getText().toString());
        entityBuilder.addTextBody("dis_city", cityID);
        entityBuilder.addTextBody("dis_state", stateID);
        entityBuilder.addTextBody("dis_aadhar", edtAdharNumber.getText().toString());
        File adharFile = saveBitmapToFile(new File(imgPathAadhar.toString()));
        Log.d(TAG, "registerDistributer: " + Integer.parseInt(String.valueOf(adharFile.length() / 1024)));
        entityBuilder.addBinaryBody("aadhar_pic", adharFile, ContentType.create("image/jpeg"), adharFile.getName());
        HttpEntity entity = entityBuilder.build();
        addCarCaller.addEntity(entity);
        addCarCaller.execute(AppGlobal.RegisterDistributer);
    }


    public void showAlertBoxwithIntent(final Context context, String msg, final Class class1) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                        Intent intent = new Intent(context, class1);
                        startActivity(intent);
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    public File saveBitmapToFile(File file) {
        try {

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE = 75;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 70, outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }

    public void permissionAccess() {
        if (ActivityCompat.checkSelfPermission(DistributorshipActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(DistributorshipActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(DistributorshipActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(DistributorshipActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(DistributorshipActivity.this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(DistributorshipActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(DistributorshipActivity.this);
                builder.setTitle("Need Storage and Camera Permission");
                builder.setMessage("This app needs storage and Camera permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(DistributorshipActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (permissionStatus.getBoolean(Manifest.permission.WRITE_EXTERNAL_STORAGE, false) || permissionStatus.getBoolean(Manifest.permission.CAMERA, false) || permissionStatus.getBoolean(Manifest.permission.READ_EXTERNAL_STORAGE, false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(DistributorshipActivity.this);
                builder.setTitle("Need Storage and Camera Permission");
                builder.setMessage("This app needs storage and Camera permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getBaseContext(), "Go to Permissions to Grant Storage and Camera", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(DistributorshipActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
            }
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.WRITE_EXTERNAL_STORAGE, true);
            editor.putBoolean(Manifest.permission.CAMERA, true);
            editor.putBoolean(Manifest.permission.READ_EXTERNAL_STORAGE, true);
            editor.commit();
        } else {
            //You already have the permission, just go ahead.
            selectImage();
        }
    }

    private void selectImage() {

        final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Upload Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                        }
                        if (photoFile != null) {
                            Uri photoURI;
                            if (Build.VERSION.SDK_INT >= 24) {
                                photoURI = FileProvider.getUriForFile(DistributorshipActivity.this,
                                        BuildConfig.APPLICATION_ID + ".provider", photoFile);
                            } else {
                                photoURI = Uri.fromFile(photoFile);
                            }
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                            startActivityForResult(takePictureIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                        }
                    }
                } else if (items[item].equals("Choose from Gallery")) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                        Intent i = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, GALLERY_INTENT_CALLED);
                    } else {
                        Intent i = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, GALLERY_INTENT_CALLED);
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        imgPathAadhar = image.getAbsolutePath();
        Log.e("Getpath", "imgPathAadhar>>>" + imgPathAadhar);

        return image;

    }

    @SuppressLint("LongLogTag")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Activity activity = (Activity) mContext;
                int targetW = imgAadhar.getWidth();
                int targetH = imgAadhar.getHeight();
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bmOptions.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imgPathAadhar, bmOptions);
                int photoW = bmOptions.outWidth;
                int photoH = bmOptions.outHeight;
                int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
                bmOptions.inJustDecodeBounds = false;
                bmOptions.inSampleSize = scaleFactor;
                bmOptions.inPurgeable = true;
                Bitmap bitmap = BitmapFactory.decodeFile(imgPathAadhar, bmOptions);
                imgAadhar.setImageBitmap(bitmap);
                Log.d("imgPathAadhar>>>", imgPathAadhar);

            }
        }
        if (requestCode == GALLERY_INTENT_CALLED && data != null) {
            Uri uri = data.getData();
            imgPathAadhar = RealPathUtil.getPath(mContext, uri);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 4;
            Bitmap bitmap = BitmapFactory.decodeFile(imgPathAadhar, options);
            imgAadhar.setImageBitmap(bitmap);
            Log.d("imagePathPanCard>>>", imgPathAadhar);

        }
    }

    public void setStateList(final ArrayList<StatePojo> mStateList) {
        Log.d(TAG, "setStateList: " + mStateList.size());
        ArrayList<String> mNameList = new ArrayList<>();
        final ArrayList<String> mCityNameList = new ArrayList<>();
        mCityNameList.add("Select City");
        mNameList.add("Select State");
        for (int i = 0; i < mStateList.size(); i++) {
            mNameList.add(mStateList.get(i).name);
        }
        ArrayAdapter<String> mAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, mNameList);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerState.setAdapter(mAdapter);

        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                String txt = adapterView.getItemAtPosition(position).toString();
                Log.d(TAG, "txt: " + txt);
                if (!txt.equals("Select State")) {
                    stateID = mStateList.get(position - 1).state_id;
                    Log.d(TAG, "stateID: " + stateID);
                    CityPojo.getCityList(mContext, stateID, "Distrubuter");
                } else {
                    stateID = "";
                    ArrayAdapter<String> mAdapter1 = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, mCityNameList);
                    mAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    Spinner_city.setAdapter(mAdapter1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void setCityList(final ArrayList<CityPojo> mCityList) {
        final ArrayList<String> mCityNameList = new ArrayList<>();
        mCityNameList.add("Select City");
        for (int i = 0; i < mCityList.size(); i++) {
            mCityNameList.add(mCityList.get(i).name);
        }
        ArrayAdapter<String> mAdapter1 = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, mCityNameList);
        mAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner_city.setAdapter(mAdapter1);
        Spinner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String txt = adapterView.getItemAtPosition(position).toString();
                Log.d(TAG, "txt: " + txt);
                if (!txt.equals("Select City")) {
                    cityID = mCityList.get(position - 1).city_id;
                    Log.d(TAG, "cityID: " + cityID);
                } else {
                    cityID = "";

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


}
