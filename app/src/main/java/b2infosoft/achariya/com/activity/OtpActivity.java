package b2infosoft.achariya.com.activity;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import b2infosoft.achariya.com.R;
import b2infosoft.achariya.com.global.AppGlobal;
import b2infosoft.achariya.com.useful.ConnectionDetector;
import b2infosoft.achariya.com.useful.UtilityMethod;

public class OtpActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView toolbar_title, tvResend;
    Context mContext;
    Button btnNext;
    EditText edtOtp;
    ConnectionDetector connectionDetector;
    private static final String TAG = "OtpActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acharya_activity_otp);

        mContext = OtpActivity.this;

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Distributor");
        toolbar_title.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        connectionDetector = new ConnectionDetector(mContext);

        tvResend = findViewById(R.id.tvResend);
        btnNext = (Button) findViewById(R.id.btnNext);
        edtOtp = findViewById(R.id.edtOtp);

        edtOtp.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        btnNext.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvResend.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (connectionDetector.isConnectingToInternet()) {
                    if (AppGlobal.Otp.equals(edtOtp.getText().toString().trim())) {
                        UtilityMethod.showAlertBoxwithIntent(mContext, "OTP Verify Success", PayUActivity.class);
                    } else {
                        UtilityMethod.showAlertBox(mContext, "Please Enter Correct Otp.");
                    }
                } else {
                    UtilityMethod.showAlertBox(mContext, "Please Enter Otp");
                }
            }
        });
    }
}
