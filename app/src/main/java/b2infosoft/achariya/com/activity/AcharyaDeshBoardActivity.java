package b2infosoft.achariya.com.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import b2infosoft.achariya.com.R;
import b2infosoft.achariya.com.adapter.CustomPagerAdapter;

public class AcharyaDeshBoardActivity extends AppCompatActivity implements View.OnClickListener {

    Context mContext;
    Toolbar toolbar;
    TextView toolbar_title;
    TextView mFirstLink;
    TextView mSecondsLink;
    TextView mThirdLink;
    TextView tvGetRsTxt, tvfastTag, tvRechargefastTag, tvDistr, tvSaveTime, tvWebPortal, tvSaveTimeDesc, tvOnlineRecharge, tvOnlineRechDesc, tvWebPortalDesc;
    LinearLayout layoutDistributorship, layoutRechargeFasttag, layoutBuyFasttag;
    ViewPager viewPager;
    ArrayList<Integer> bannerList = new ArrayList<>();
    Timer timer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acharya_activity_desh_board);
        mContext = AcharyaDeshBoardActivity.this;
        initView();
        swipeLayout();
    }

    private void swipeLayout() {

        bannerList.add(R.drawable.banner_a);
        bannerList.add(R.drawable.banner_b);
        bannerList.add(R.drawable.banner_c);

        viewPager = (ViewPager) findViewById(R.id.pager);
        CustomPagerAdapter adapter = new CustomPagerAdapter(AcharyaDeshBoardActivity.this, bannerList, mContext);
        viewPager.setAdapter(adapter);
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                viewPager.post(new Runnable() {
                    @Override
                    public void run() {
                        viewPager.setCurrentItem((viewPager.getCurrentItem() + 1) % bannerList.size());
                    }
                });
            }
        };
        timer = new Timer();
        timer.schedule(timerTask, 3000, 3000);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mFirstLink = (TextView) toolbar.findViewById(R.id.mFirstLink);

        mSecondsLink = (TextView) toolbar.findViewById(R.id.mSecondsLink);
        mThirdLink = (TextView) toolbar.findViewById(R.id.mThirdLink);

        mFirstLink.setVisibility(View.VISIBLE);
        mSecondsLink.setVisibility(View.VISIBLE);
        mThirdLink.setVisibility(View.VISIBLE);


        //toolbar_title.setText("FASTag");
        toolbar_title.setText("Achariya Group");
        //toolbar_title.setTypeface(Typeface.createFromAsset(getAssets(), "font/kurti_dev.ttf"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tvDistr = (TextView) findViewById(R.id.tvDistr);
        tvGetRsTxt = (TextView) findViewById(R.id.tvGetRsTxt);
        tvRechargefastTag = (TextView) findViewById(R.id.tvRechargefastTag);
        tvfastTag = (TextView) findViewById(R.id.tvfastTag);
        tvOnlineRechDesc = (TextView) findViewById(R.id.tvOnlineRechDesc);
        tvSaveTimeDesc = (TextView) findViewById(R.id.tvSaveTimeDesc);
        tvOnlineRecharge = (TextView) findViewById(R.id.tvOnlineRecharge);
        tvSaveTime = (TextView) findViewById(R.id.tvSaveTime);
        tvWebPortalDesc = (TextView) findViewById(R.id.tvWebPortalDesc);
        tvWebPortal = (TextView) findViewById(R.id.tvWebPortal);
        layoutRechargeFasttag = (LinearLayout) findViewById(R.id.layoutRechargeFasttag);
        layoutDistributorship = (LinearLayout) findViewById(R.id.layoutDistributorship);
        layoutBuyFasttag = (LinearLayout) findViewById(R.id.layoutBuyFasttag);
        tvGetRsTxt.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));
        tvfastTag.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvRechargefastTag.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvDistr.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvSaveTime.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvOnlineRecharge.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvWebPortal.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semibold.ttf"));
        tvSaveTimeDesc.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvOnlineRechDesc.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));
        tvWebPortalDesc.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_regular.ttf"));


        mFirstLink.setVisibility(View.GONE);
        mSecondsLink.setVisibility(View.GONE);
        mThirdLink.setVisibility(View.GONE);
        layoutBuyFasttag.setOnClickListener(this);
        layoutRechargeFasttag.setOnClickListener(this);
        layoutDistributorship.setOnClickListener(this);
        mFirstLink.setOnClickListener(this);
        mSecondsLink.setOnClickListener(this);
        mThirdLink.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layoutBuyFasttag:
                startActivity(new Intent(mContext, RegisterBasicInfoActivity.class));
                break;
            case R.id.layoutDistributorship:
                startActivity(new Intent(mContext, DistributorshipActivity.class));
                break;
            case R.id.layoutRechargeFasttag:
                startActivity(new Intent(mContext, RechargeFastagActivity.class));

                break;
            case R.id.mFirstLink:
                intentOnBrowser("http://solar.achariyagroup.com/");

                break;
            case R.id.mSecondsLink:
                intentOnBrowser("http://emitra.achariyagroup.com/");

                break;
            case R.id.mThirdLink:
                intentOnBrowser("http://fastag.achariyagroup.com/");

                break;
        }
    }

    private void intentOnBrowser(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

}
