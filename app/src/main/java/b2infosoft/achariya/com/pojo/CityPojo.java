package b2infosoft.achariya.com.pojo;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import b2infosoft.achariya.com.activity.BasicInfoSecondActivity;
import b2infosoft.achariya.com.activity.DistributorshipActivity;
import b2infosoft.achariya.com.global.AppGlobal;
import b2infosoft.achariya.com.webservice.WebServiceCaller;

/**
 * Created by u on 14-Mar-18.
 */

public class CityPojo {

    public String city_id = "";
    public String name = "";
    private static final String TAG = "CityPojo";

    public CityPojo(String city_id, String name) {
        this.city_id = city_id;
        this.name = name;
    }

    public static void getCityList(final Context mContext, String stateID, final String fromWhere) {

        final ArrayList<CityPojo> mCityList = new ArrayList<>();
        WebServiceCaller webServiceCaller = new WebServiceCaller(WebServiceCaller.GET_TASK, mContext, "Please Wait...", true) {
            @Override
            public void handleResponse(String response) {
                Log.d(TAG, "handleResponse: " + response);

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject mDataObject = jsonArray.getJSONObject(i);
                        mCityList.add(new CityPojo(mDataObject.getString("city_id"),
                                mDataObject.getString("name")));
                    }
                    if (!mCityList.isEmpty()) {
                        if (fromWhere.equals("Distrubuter")) {
                            ((DistributorshipActivity) mContext).setCityList(mCityList);

                        } else {
                            ((BasicInfoSecondActivity) mContext).setCityList(mCityList);

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        webServiceCaller.execute(AppGlobal.GetCity.replace("@state_id", stateID));

    }


}
