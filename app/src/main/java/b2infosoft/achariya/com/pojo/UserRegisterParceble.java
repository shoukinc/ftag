package b2infosoft.achariya.com.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by u on 14-Mar-18.
 */

public class UserRegisterParceble implements Parcelable {

    public UserRegisterParceble() {

    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAadhar() {
        return aadhar;
    }

    public void setAadhar(String aadhar) {
        this.aadhar = aadhar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    String mobileNumber = "";
    String first_name = "";
    String last_name = "";
    String gender = "";
    String address = "";
    String aadhar = "";
    String email = "";
    String dob = "";
    String state = "";
    String city = "";
    String pincode = "";

    public UserRegisterParceble(Parcel in) {
        mobileNumber = in.readString();
        first_name = in.readString();
        last_name = in.readString();
        gender = in.readString();
        address = in.readString();
        aadhar = in.readString();
        email = in.readString();
        dob = in.readString();
        state = in.readString();
        city = in.readString();
        pincode = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mobileNumber);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(gender);
        dest.writeString(address);
        dest.writeString(aadhar);
        dest.writeString(email);
        dest.writeString(dob);
        dest.writeString(state);
        dest.writeString(city);
        dest.writeString(pincode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserRegisterParceble> CREATOR = new Creator<UserRegisterParceble>() {
        @Override
        public UserRegisterParceble createFromParcel(Parcel in) {
            return new UserRegisterParceble(in);
        }

        @Override
        public UserRegisterParceble[] newArray(int size) {
            return new UserRegisterParceble[size];
        }
    };
}
