package b2infosoft.achariya.com.pojo;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import b2infosoft.achariya.com.activity.BasicInfoSecondActivity;
import b2infosoft.achariya.com.activity.DistributorshipActivity;
import b2infosoft.achariya.com.global.AppGlobal;
import b2infosoft.achariya.com.webservice.WebServiceCaller;

/**
 * Created by u on 14-Mar-18.
 */

public class StatePojo {

    public String state_id = "";
    public String name = "";
    private static final String TAG = "StatePojo";

    public StatePojo(String state_id, String name) {
        this.state_id = state_id;
        this.name = name;
    }


    public static void getStateList(final Context mContext, final String fromWhere) {
        final ArrayList<StatePojo> mStateList = new ArrayList<>();
        WebServiceCaller webServiceCaller = new WebServiceCaller(WebServiceCaller.GET_TASK, mContext, "Please Wait...", true) {
            @Override
            public void handleResponse(String response) {
                Log.d(TAG, "handleResponse: " + response);

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject mDataObject = jsonArray.getJSONObject(i);
                        mStateList.add(new StatePojo(mDataObject.getString("state_id"),
                                mDataObject.getString("name")));
                    }
                    if (!mStateList.isEmpty()) {
                        if (fromWhere.equals("Distributer")){
                            ((DistributorshipActivity) mContext).setStateList(mStateList);
                        }else{
                            ((BasicInfoSecondActivity) mContext).setStateList(mStateList);

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        webServiceCaller.execute(AppGlobal.getStates.replace("@state", "states"));


    }


}
