package b2infosoft.achariya.com.pojo;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import b2infosoft.achariya.com.activity.RechargeFastagActivity;
import b2infosoft.achariya.com.activity.RegisterDocumentUploadActivity;
import b2infosoft.achariya.com.global.AppGlobal;
import b2infosoft.achariya.com.webservice.WebServiceCaller;

/**
 * Created by u on 14-Mar-18.
 */

public class ClassesPojo {

    private static final String TAG = "ClassesPojo";
    public String class_id = "";
    public String class_name = "";
    public String class_price = "";
    public String addedon = "";
    public String status = "";
    public String date = "";

    public ClassesPojo(String class_id, String class_name, String class_price, String addedon, String status, String date) {
        this.class_id = class_id;
        this.class_name = class_name;
        this.class_price = class_price;
        this.addedon = addedon;
        this.status = status;
        this.date = date;
    }


    public static void getClassesList(final Context mContext, final String fromWhere) {

        final ArrayList<ClassesPojo> mClassesList = new ArrayList<>();
        WebServiceCaller webServiceCaller = new WebServiceCaller(WebServiceCaller.GET_TASK, mContext, "Please Wait...", true) {
            @Override
            public void handleResponse(String response) {
                Log.d(TAG, "handleResponse: " + response);

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject mDataObject = jsonArray.getJSONObject(i);
                        mClassesList.add(new ClassesPojo(mDataObject.getString("class_id"),
                                mDataObject.getString("class_name"),
                                mDataObject.getString("class_price"),
                                mDataObject.getString("addedon"),
                                mDataObject.getString("status"),
                                mDataObject.getString("date")));
                    }
                    if (!mClassesList.isEmpty()) {
                        if (fromWhere.equals("RechargeFastagActivity")) {
                            ((RechargeFastagActivity) mContext).setClassesList(mClassesList);
                        } else {
                            ((RegisterDocumentUploadActivity) mContext).setClassesList(mClassesList);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        webServiceCaller.execute(AppGlobal.GetClasses.replace("@prices", "classes"));

    }


}
