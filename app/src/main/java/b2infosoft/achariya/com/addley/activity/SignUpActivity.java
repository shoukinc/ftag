package b2infosoft.achariya.com.addley.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import b2infosoft.achariya.com.R;
import b2infosoft.achariya.com.global.AppGlobal;
import b2infosoft.achariya.com.useful.UtilityMethod;


public class SignUpActivity extends AppCompatActivity {

    EditText sign_up_name, sign_up_email, sign_up_mobile, sign_up_OfficeAdd, edtFees;
    Button sign_up_NextButton;
    Context mContext;
    Toolbar toolbar;
    TextView toolbar_title;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String fromWhere = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__signup);
        mContext = SignUpActivity.this;
        initView();


    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("SIGN UP");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Drawable upArrow = ContextCompat.getDrawable(mContext, R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(ContextCompat.getColor(mContext, R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        sign_up_name = (EditText) findViewById(R.id.sign_up_name);
        sign_up_email = (EditText) findViewById(R.id.sign_up_email);
        sign_up_mobile = (EditText) findViewById(R.id.sign_up_mobile);
        sign_up_OfficeAdd = (EditText) findViewById(R.id.sign_up_OfficeAdd);
        edtFees = (EditText) findViewById(R.id.edtFees);
        sign_up_NextButton = (Button) findViewById(R.id.sign_up_NextButton);
        sign_up_name.setText(AppGlobal.Name);
        sign_up_mobile.setText(AppGlobal.Mobile);
        sign_up_OfficeAdd.setText(AppGlobal.Address);
        sign_up_email.setText(AppGlobal.Email);
        edtFees.setText(AppGlobal.Fees);







        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        if (intent != null) {
            fromWhere = intent.getStringExtra("fromWhere");
        }
        sign_up_NextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sign_up_name.getText().toString().trim().equals("") || sign_up_email.getText().toString().trim().equals("") || sign_up_mobile.getText().toString().trim().equals("") || sign_up_OfficeAdd.getText().toString().trim().equals("")) {
                    UtilityMethod.showAlertBox(mContext, "Please enter all field.");
                } else {
                    if (!sign_up_email.getText().toString().matches(emailPattern)) {
                        UtilityMethod.showAlertBox(mContext, "Invalid email address.");
                    } else if (sign_up_mobile.getText().toString().length() != 10) {
                        UtilityMethod.showAlertBox(mContext, "Invalid mobile number.");
                    } else {
                        Log.d("fromWhere>>>", fromWhere);
                        Intent intent = new Intent(mContext, UploadDocsActivity.class);
                        intent.putExtra("Name", sign_up_name.getText().toString());
                        intent.putExtra("Email", sign_up_email.getText().toString());
                        intent.putExtra("Mobile", sign_up_mobile.getText().toString());
                        intent.putExtra("Address", sign_up_OfficeAdd.getText().toString());
                        intent.putExtra("fromWhere", fromWhere);
                        intent.putExtra("fees", edtFees.getText().toString());
                        startActivity(intent);
                    }
                }
            }
        });

    }

    /**
     * Created by b2andro on 2/6/2018.
     */

    public static class Thank_you extends AppCompatActivity {


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }
    }
}
