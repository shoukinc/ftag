package b2infosoft.achariya.com.addley.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import b2infosoft.achariya.com.R;
import b2infosoft.achariya.com.global.AppGlobal;
import b2infosoft.achariya.com.useful.UtilityMethod;


public class FastTagSignUpActivity extends AppCompatActivity {


    Button Fastag_sign_up_NextButton;
    Context mContext;
    Toolbar toolbar;
    TextView toolbar_title;
    EditText Fastag_sign_up_name, Fastag_sign_up_email, Fastag_sign_up_mobile, Fastag_sign_up_OfficeAdd;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String fromWhere = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fast_tag__signup);
        initView();


    }

    private void initView() {
        mContext = FastTagSignUpActivity.this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("SIGN UP");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Drawable upArrow = ContextCompat.getDrawable(mContext, R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(ContextCompat.getColor(mContext, R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        Fastag_sign_up_name = (EditText) findViewById(R.id.Fastag_sign_up_name);
        Fastag_sign_up_email = (EditText) findViewById(R.id.Fastag_sign_up_email);
        Fastag_sign_up_mobile = (EditText) findViewById(R.id.Fastag_sign_up_mobile);
        Fastag_sign_up_OfficeAdd = (EditText) findViewById(R.id.Fastag_sign_up_OfficeAdd);
        Fastag_sign_up_name.setText(AppGlobal.Name);
        Fastag_sign_up_mobile.setText(AppGlobal.Mobile);
        Fastag_sign_up_OfficeAdd.setText(AppGlobal.Address);
        Fastag_sign_up_email.setText(AppGlobal.Email);


        Fastag_sign_up_NextButton = (Button) findViewById(R.id.Fastag_sign_up_NextButton);
        Fastag_sign_up_NextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent intentfast = new Intent(FastTagSignUpActivity.this, FastTagUploadDocsActivity.class);
                startActivity(intentfast);*/

                if (Fastag_sign_up_name.getText().toString().trim().equals("") || Fastag_sign_up_email.getText().toString().trim().equals("") || Fastag_sign_up_mobile.getText().toString().trim().equals("") || Fastag_sign_up_OfficeAdd.getText().toString().trim().equals("")) {
                    UtilityMethod.showAlertBox(mContext, "Please enter all field.");
                } else {
                    if (!Fastag_sign_up_email.getText().toString().matches(emailPattern)) {
                        UtilityMethod.showAlertBox(mContext, "Invalid email address.");
                    } else if (Fastag_sign_up_mobile.getText().toString().length() != 10) {
                        UtilityMethod.showAlertBox(mContext, "Invalid mobile number.");
                    } else {
                        Intent intent = new Intent(mContext, FastTagUploadDocsActivity.class);
                        intent.putExtra("Name", Fastag_sign_up_name.getText().toString());
                        intent.putExtra("Email", Fastag_sign_up_email.getText().toString());
                        intent.putExtra("Mobile", Fastag_sign_up_mobile.getText().toString());
                        intent.putExtra("Address", Fastag_sign_up_OfficeAdd.getText().toString());
                        startActivity(intent);
                    }
                }
            }
        });
    }
}
