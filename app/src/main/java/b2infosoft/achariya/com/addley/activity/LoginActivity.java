package b2infosoft.achariya.com.addley.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;


import org.json.JSONException;
import org.json.JSONObject;

import b2infosoft.achariya.com.R;
import b2infosoft.achariya.com.global.AppGlobal;
import b2infosoft.achariya.com.sharedPreference.SessionManager;
import b2infosoft.achariya.com.useful.UtilityMethod;
import b2infosoft.achariya.com.webservice.WebServiceCaller;


public class LoginActivity extends AppCompatActivity {


    EditText sign_up_name, sign_up_mobile, Edt_Confm_otp;
    TextView txt_Resend;
    LinearLayout linear_layout1, linear_layout2;
    Button Btn_Send_otp, Btn_Confirm;
    Context mContext;
    Toolbar toolbar;
    TextView toolbar_title;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__login);
        mContext = LoginActivity.this;
        initView();

    }

    private void initView() {
        sessionManager = new SessionManager(mContext);

        if (sessionManager.isLoggedIn() == true) {
            UtilityMethod.goNextClass(mContext, DashboardActivity.class);
        }


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("LOGIN");

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Drawable upArrow = ContextCompat.getDrawable(mContext, R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(ContextCompat.getColor(mContext, R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
     /*   getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle("LOGIN");*/

        sign_up_name = (EditText) findViewById(R.id.sign_up_name);
        sign_up_mobile = (EditText) findViewById(R.id.sign_up_mobile);
        Edt_Confm_otp = (EditText) findViewById(R.id.Edt_Confm_otp);
        linear_layout1 = (LinearLayout) findViewById(R.id.linear_layout1);
        linear_layout2 = (LinearLayout) findViewById(R.id.linear_layout2);

        txt_Resend = (TextView) findViewById(R.id.txt_Resend);

        Btn_Send_otp = (Button) findViewById(R.id.Btn_Send_otp);
        Btn_Confirm = (Button) findViewById(R.id.Btn_Confirm);

        Btn_Send_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sign_up_name.getText().toString().trim().equals("") || sign_up_mobile.getText().toString().trim().equals("")) {
                    UtilityMethod.showAlertBox(mContext, "Please fill the blank field");
                } else {
                    if (sign_up_mobile.getText().toString().trim().length() == 10) {
                        WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait...", true) {
                            @Override
                            public void handleResponse(String response) {
                                Log.d("Response>>>>", response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getString("success").equals("true")) {
                                        AppGlobal.Name = sign_up_name.getText().toString();
                                        AppGlobal.Mobile = sign_up_mobile.getText().toString();
                                        String id = jsonObject.getString("user_id");
                                       /* String name = jsonObject.getString("name");
                                        String mobile = jsonObject.getString("mobile");*/
                                        sessionManager.createLoginSession(AppGlobal.Name, AppGlobal.Mobile, id);
                                        UtilityMethod.showAlertBoxwithIntent(mContext, "Saved Successfully", DashboardActivity.class);
                                    } else {
                                        UtilityMethod.showAlertBox(mContext, "Data Saving Failed");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        };
                        serviceCaller.addNameValuePair("name", sign_up_name.getText().toString());
                        serviceCaller.addNameValuePair("mobile", sign_up_mobile.getText().toString());
                        serviceCaller.execute(AppGlobal.AddleyCheckUser);
                    } else {
                        UtilityMethod.showAlertBox(mContext, "Invalid mobile number");
                    }



                   /* linear_layout1.setVisibility(View.GONE);
                    linear_layout2.setVisibility(View.GONE);*/
                }


            }
        });

        Btn_Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Edt_Confm_otp.getText().toString().trim().equals("")) {
                    UtilityMethod.showAlertBox(mContext, "Please fill the blank field");
                } else {
                    Intent intentLogin = new Intent(LoginActivity.this, DashboardActivity.class);
                    startActivity(intentLogin);
                }
            }
        });
    }
}
