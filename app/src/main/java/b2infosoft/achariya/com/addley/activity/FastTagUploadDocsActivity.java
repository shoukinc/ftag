package b2infosoft.achariya.com.addley.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import b2infosoft.achariya.com.BuildConfig;
import b2infosoft.achariya.com.R;
import b2infosoft.achariya.com.global.AppGlobal;
import b2infosoft.achariya.com.sharedPreference.SessionManager;
import b2infosoft.achariya.com.useful.RealPathUtil;
import b2infosoft.achariya.com.useful.UtilityMethod;
import b2infosoft.achariya.com.webservice.WebServiceCaller;


public class FastTagUploadDocsActivity extends AppCompatActivity implements View.OnClickListener {
    Context mContext;
    Toolbar toolbar;
    TextView toolbar_title;
    ImageView Registration_Certificate_img, kyc_Doc_img, Address_Proof_img, Passport_Size_img, Driving_License_img, Voter_Id_img,
            Adharcard_img_fastag;
    String imagePathRegistration_C = "", imagePathKYC = "", imagePathAddress_Proof = "", imagePathPassportSizephoto = "",
            imagePathDriving_License = "", imagePathVoter_Id = "", imagePathAdharcard = "";
    String imageView = "";
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private SharedPreferences permissionStatus;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 101;
    private static final int GALLERY_INTENT_CALLED = 201;
    private boolean sentToSettings = false;
    String Name = "", Email = "", Mobile = "", Address = "";
    Button btnUpload;
    SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fastag_upload__doc);
        mContext = FastTagUploadDocsActivity.this;
        initView();
    }

    private void initView() {
        sessionManager = new SessionManager(mContext);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("UPLOAD DOCUMENT");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Drawable upArrow = ContextCompat.getDrawable(mContext, R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(ContextCompat.getColor(mContext, R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);
        Registration_Certificate_img = (ImageView) findViewById(R.id.Registration_Certificate_img);
        kyc_Doc_img = (ImageView) findViewById(R.id.kyc_Doc_img);
        Address_Proof_img = (ImageView) findViewById(R.id.Address_Proof_img);
        Passport_Size_img = (ImageView) findViewById(R.id.Passport_Size_img);
        Driving_License_img = (ImageView) findViewById(R.id.Driving_License_img);
        Adharcard_img_fastag = (ImageView) findViewById(R.id.Adharcard_img_fastag);
        Voter_Id_img = (ImageView) findViewById(R.id.Voter_Id_img);
        btnUpload = (Button) findViewById(R.id.btnUpload);

        Registration_Certificate_img.setOnClickListener(this);
        kyc_Doc_img.setOnClickListener(this);
        Address_Proof_img.setOnClickListener(this);
        Passport_Size_img.setOnClickListener(this);
        Driving_License_img.setOnClickListener(this);
        Adharcard_img_fastag.setOnClickListener(this);
        Voter_Id_img.setOnClickListener(this);
        btnUpload.setOnClickListener(this);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Intent intent = getIntent();
        if (intent != null) {
            Name = intent.getStringExtra("Name");
            Mobile = intent.getStringExtra("Mobile");
            Address = intent.getStringExtra("Address");
            Email = intent.getStringExtra("Email");
            Log.d("UserInfo>>>", "" + "::" + Name + "::" + Mobile + "::" + Address + "::" + Email);
        }


        if (!AppGlobal.Reg_certificate.equals("")) {
            Glide.with(this).load(AppGlobal.AddleyImageUrl + AppGlobal.Reg_certificate).into(Registration_Certificate_img);
            //imagePathRegistration_C = AppGlobal.Reg_certificate;
        }
        if (!AppGlobal.Kyc_document.equals("")) {
            Glide.with(this).load(AppGlobal.AddleyImageUrl + AppGlobal.Kyc_document).into(kyc_Doc_img);
            //imagePathKYC = AppGlobal.Kyc_document;
        }

        if (!AppGlobal.Address_Proof.equals("")) {
            Glide.with(this).load(AppGlobal.AddleyImageUrl + AppGlobal.Address_Proof).into(Address_Proof_img);
            // imagePathAddress_Proof = AppGlobal.Address_Proof;
        }

        if (!AppGlobal.Passport_pic.equals("")) {
            Glide.with(this).load(AppGlobal.AddleyImageUrl + AppGlobal.Passport_pic).into(Passport_Size_img);
            //  imagePathPassportSizephoto = AppGlobal.Passport_pic;
        }

        if (!AppGlobal.Driving_license.equals("")) {
            Glide.with(this).load(AppGlobal.AddleyImageUrl + AppGlobal.Driving_license).into(Driving_License_img);
            // imagePathDriving_License = AppGlobal.Driving_license;
        }
        if (!AppGlobal.Voter_card.equals("")) {
            Glide.with(this).load(AppGlobal.AddleyImageUrl + AppGlobal.Voter_card).into(Voter_Id_img);
            //  imagePathVoter_Id = AppGlobal.Voter_card;
        }
        if (!AppGlobal.Aadhar_card.equals("")) {
            Glide.with(this).load(AppGlobal.AddleyImageUrl + AppGlobal.Aadhar_card).into(Adharcard_img_fastag);
            //  imagePathAdharcard = AppGlobal.Aadhar_card;
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.Registration_Certificate_img:
                imageView = "Registration_Certificate_img";
                permissionAccess();

                break;
            case R.id.kyc_Doc_img:
                imageView = "kyc_Doc_img";
                permissionAccess();
                break;


            case R.id.Address_Proof_img:
                imageView = "Address_Proof_img";
                permissionAccess();
                break;


            case R.id.Passport_Size_img:
                imageView = "Passport_Size_img";
                permissionAccess();
                break;


            case R.id.Driving_License_img:
                imageView = "Driving_License_img";
                permissionAccess();
                break;


            case R.id.Voter_Id_img:
                imageView = "Voter_Id_img";
                permissionAccess();
                break;

            case R.id.Adharcard_img_fastag:
                imageView = "Adharcard_img_fastag";
                permissionAccess();
                break;
            case R.id.btnUpload:
                /*if (!imagePathAdharcard.equals("") && !imagePathRegistration_C.equals("") && !imagePathKYC.equals("") && !imagePathAddress_Proof.equals("") && !imagePathPassportSizephoto.equals("") && !imagePathDriving_License.equals("") && !imagePathVoter_Id.equals("")) {*/
                if (AppGlobal.UserType.equals("New")) {
                    newRegister();
                } else {
                    updateData();
                }

                /*} else {
                    UtilityMethod.showAlertBox(mContext, "Please Select All Documents.");
                }*/
                break;
        }

    }

    private void updateData() {
        WebServiceCaller caller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please Wait...", true) {
            @Override
            public void handleResponse(String response) {
                String successStr = "";
                Log.d("Respose---->>>>", response);
                try {
                    JSONObject mainObj = new JSONObject(response);
                    if (mainObj.getString("success").equals("true")) {
                        UtilityMethod.showAlertBoxwithIntent(mContext, "Data Uploaded Successfully", Thank_you_Activity.class);
                    } else {
                        UtilityMethod.showAlertBox(mContext, mainObj.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
        entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        entityBuilder.addTextBody("name", Name);
        entityBuilder.addTextBody("mobile", Mobile);
        entityBuilder.addTextBody("email", Email);
        entityBuilder.addTextBody("address", Address);
        entityBuilder.addTextBody("user_type_id", "5");
        entityBuilder.addTextBody("user_id", sessionManager.getSessionValue(SessionManager.KEY_ID));
        if (imagePathRegistration_C.equals("")) {
        } else {
            Log.d("reg_certificate>>>>", "" + new File(imagePathRegistration_C).getName());
            entityBuilder.addBinaryBody("reg_certificate", new File(imagePathRegistration_C), ContentType.create("image/jpeg"), new File(imagePathRegistration_C).getName());
        }
        if (imagePathKYC.equals("")) {
        } else {
            Log.d("kyc_document>>>>", "" + new File(imagePathKYC).getName());
            entityBuilder.addBinaryBody("kyc_document", new File(imagePathKYC), ContentType.create("image/jpeg"), new File(imagePathKYC).getName());
        }
        if (imagePathAddress_Proof.equals("")) {
        } else {
            Log.d("address_Proof>>>>", "" + new File(imagePathAddress_Proof).getName());
            entityBuilder.addBinaryBody("address_Proof", new File(imagePathAddress_Proof), ContentType.create("image/jpeg"), new File(imagePathAddress_Proof).getName());
        }
        if (imagePathPassportSizephoto.equals("")) {
        } else {
            Log.d("photo>>>>", "" + new File(imagePathPassportSizephoto).getName());
            entityBuilder.addBinaryBody("photo", new File(imagePathPassportSizephoto), ContentType.create("image/jpeg"), new File(imagePathPassportSizephoto).getName());
        }
        if (imagePathDriving_License.equals("")) {
        } else {
            Log.d("driving_license>>>>", "" + new File(imagePathDriving_License).getName());
            entityBuilder.addBinaryBody("driving_license", new File(imagePathDriving_License), ContentType.create("image/jpeg"), new File(imagePathDriving_License).getName());
        }
        if (imagePathVoter_Id.equals("")) {
        } else {
            Log.d("voter_card>>>>", "" + new File(imagePathVoter_Id).getName());
            entityBuilder.addBinaryBody("voter_card", new File(imagePathVoter_Id), ContentType.create("image/jpeg"), new File(imagePathVoter_Id).getName());
        }
        if (imagePathAdharcard.equals("")) {
        } else {
            Log.d("aadhar_card>>>>", "" + new File(imagePathAdharcard).getName());
            entityBuilder.addBinaryBody("aadhar_card", new File(imagePathAdharcard), ContentType.create("image/jpeg"), new File(imagePathAdharcard).getName());
        }
        HttpEntity entity = entityBuilder.build();
        caller.addEntity(entity);
        caller.execute(AppGlobal.AddleyUpdateUserRegister);
    }

    public void newRegister() {
        WebServiceCaller caller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please Wait...", true) {
            @Override
            public void handleResponse(String response) {
                String successStr = "";
                Log.d("Respose---->>>>", response);
                try {
                    JSONObject mainObj = new JSONObject(response);
                    if (mainObj.getString("success").equals("true")) {
                        UtilityMethod.showAlertBoxwithIntent(mContext, "Data Uploaded Successfully", Thank_you_Activity.class);
                    } else {
                        UtilityMethod.showAlertBox(mContext, mainObj.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
        entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        entityBuilder.addTextBody("name", Name);
        entityBuilder.addTextBody("mobile", Mobile);
        entityBuilder.addTextBody("email", Email);
        entityBuilder.addTextBody("address", Address);
        entityBuilder.addTextBody("user_type_id", "5");
        entityBuilder.addTextBody("user_id", "" + sessionManager.getSessionValue(SessionManager.KEY_ID));

        if (imagePathRegistration_C.equals("")) {
        } else {
            Log.d("reg_certificate>>>>", "" + new File(imagePathRegistration_C).getName());
            entityBuilder.addBinaryBody("reg_certificate", new File(imagePathRegistration_C), ContentType.create("image/jpeg"), new File(imagePathRegistration_C).getName());
        }
        if (imagePathKYC.equals("")) {
        } else {
            Log.d("kyc_document>>>>", "" + new File(imagePathKYC).getName());
            entityBuilder.addBinaryBody("kyc_document", new File(imagePathKYC), ContentType.create("image/jpeg"), new File(imagePathKYC).getName());
        }
        if (imagePathAddress_Proof.equals("")) {
        } else {
            Log.d("address_Proof>>>>", "" + new File(imagePathAddress_Proof).getName());
            entityBuilder.addBinaryBody("address_Proof", new File(imagePathAddress_Proof), ContentType.create("image/jpeg"), new File(imagePathAddress_Proof).getName());
        }
        if (imagePathPassportSizephoto.equals("")) {
        } else {
            Log.d("photo>>>>", "" + new File(imagePathPassportSizephoto).getName());
            entityBuilder.addBinaryBody("photo", new File(imagePathPassportSizephoto), ContentType.create("image/jpeg"), new File(imagePathPassportSizephoto).getName());
        }
        if (imagePathDriving_License.equals("")) {
        } else {
            Log.d("driving_license>>>>", "" + new File(imagePathDriving_License).getName());
            entityBuilder.addBinaryBody("driving_license", new File(imagePathDriving_License), ContentType.create("image/jpeg"), new File(imagePathDriving_License).getName());
        }
        if (imagePathVoter_Id.equals("")) {
        } else {
            Log.d("voter_card>>>>", "" + new File(imagePathVoter_Id).getName());
            entityBuilder.addBinaryBody("voter_card", new File(imagePathVoter_Id), ContentType.create("image/jpeg"), new File(imagePathVoter_Id).getName());
        }
        if (imagePathAdharcard.equals("")) {
        } else {
            Log.d("aadhar_card>>>>", "" + new File(imagePathAdharcard).getName());
            entityBuilder.addBinaryBody("aadhar_card", new File(imagePathAdharcard), ContentType.create("image/jpeg"), new File(imagePathAdharcard).getName());
        }
        HttpEntity entity = entityBuilder.build();
        caller.addEntity(entity);
        caller.execute(AppGlobal.AddleyRegisterUser);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == EXTERNAL_STORAGE_PERMISSION_CONSTANT) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //The External Storage Write Permission is granted to you... Continue your left job...
                selectImage();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(FastTagUploadDocsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(FastTagUploadDocsActivity.this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(FastTagUploadDocsActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    //Show Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(FastTagUploadDocsActivity.this);
                    builder.setTitle("Need Storage and Camera Permission");
                    builder.setMessage("This app needs storage and Camera permission");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            ActivityCompat.requestPermissions(FastTagUploadDocsActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    Toast.makeText(getBaseContext(), "Unable to get Permission", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public void permissionAccess() {
        if (ActivityCompat.checkSelfPermission(FastTagUploadDocsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(FastTagUploadDocsActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(FastTagUploadDocsActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(FastTagUploadDocsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(FastTagUploadDocsActivity.this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(FastTagUploadDocsActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(FastTagUploadDocsActivity.this);
                builder.setTitle("Need Storage and Camera Permission");
                builder.setMessage("This app needs storage and Camera permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(FastTagUploadDocsActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (permissionStatus.getBoolean(Manifest.permission.WRITE_EXTERNAL_STORAGE, false) || permissionStatus.getBoolean(Manifest.permission.CAMERA, false) || permissionStatus.getBoolean(Manifest.permission.READ_EXTERNAL_STORAGE, false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(FastTagUploadDocsActivity.this);
                builder.setTitle("Need Storage and Camera Permission");
                builder.setMessage("This app needs storage and Camera permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getBaseContext(), "Go to Permissions to Grant Storage and Camera", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(FastTagUploadDocsActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
            }
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.WRITE_EXTERNAL_STORAGE, true);
            editor.putBoolean(Manifest.permission.CAMERA, true);
            editor.putBoolean(Manifest.permission.READ_EXTERNAL_STORAGE, true);
            editor.commit();
        } else {
            //You already have the permission, just go ahead.
            selectImage();
        }
    }

    private void selectImage() {

        final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Upload Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                        }
                        if (photoFile != null) {
                            Uri photoURI;
                            if (Build.VERSION.SDK_INT >= 24) {
                                photoURI = FileProvider.getUriForFile(FastTagUploadDocsActivity.this,
                                        BuildConfig.APPLICATION_ID + ".provider", photoFile);
                            } else {
                                photoURI = Uri.fromFile(photoFile);
                            }
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                            startActivityForResult(takePictureIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                        }
                    }
                } else if (items[item].equals("Choose from Gallery")) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                        Intent i = new Intent(
                                Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, GALLERY_INTENT_CALLED);
                    } else {
                        Intent i = new Intent(
                                Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, GALLERY_INTENT_CALLED);
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        if (imageView.equals("Registration_Certificate_img")) {
            imagePathRegistration_C = image.getAbsolutePath();
            Log.e("Getpath", "Cool" + imagePathRegistration_C);
        } else if (imageView.equals("kyc_Doc_img")) {
            imagePathKYC = image.getAbsolutePath();
            Log.e("Getpath", "Cool" + imagePathKYC);
        } else if (imageView.equals("Address_Proof_img")) {
            imagePathAddress_Proof = image.getAbsolutePath();
            Log.e("Getpath", "Cool" + imagePathAddress_Proof);
        } else if (imageView.equals("Passport_Size_img")) {
            imagePathPassportSizephoto = image.getAbsolutePath();
            Log.e("Getpath", "Cool" + imagePathPassportSizephoto);
        } else if (imageView.equals("Driving_License_img")) {
            imagePathDriving_License = image.getAbsolutePath();
            Log.e("Getpath", "Cool" + imagePathDriving_License);
        } else if (imageView.equals("Voter_Id_img")) {
            imagePathVoter_Id = image.getAbsolutePath();
            Log.e("Getpath", "Cool" + imagePathVoter_Id);
        } else if (imageView.equals("Adharcard_img_fastag")) {
            imagePathAdharcard = image.getAbsolutePath();
            Log.e("Getpath", "Cool" + imagePathAdharcard);
        }
        return image;

    }

    @SuppressLint("LongLogTag")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Activity activity = (Activity) mContext;
                if (imageView.equals("Registration_Certificate_img")) {
                    int targetW = Registration_Certificate_img.getWidth();
                    int targetH = Registration_Certificate_img.getHeight();

                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(imagePathRegistration_C, bmOptions);
                    int photoW = bmOptions.outWidth;
                    int photoH = bmOptions.outHeight;
                    int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
                    bmOptions.inJustDecodeBounds = false;
                    bmOptions.inSampleSize = scaleFactor;
                    bmOptions.inPurgeable = true;
                    Bitmap bitmap = BitmapFactory.decodeFile(imagePathRegistration_C, bmOptions);
                    Registration_Certificate_img.setImageBitmap(bitmap);

                } else if (imageView.equals("kyc_Doc_img")) {
                    int targetW = kyc_Doc_img.getWidth();
                    int targetH = kyc_Doc_img.getHeight();
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(imagePathKYC, bmOptions);
                    int photoW = bmOptions.outWidth;
                    int photoH = bmOptions.outHeight;
                    int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
                    bmOptions.inJustDecodeBounds = false;
                    bmOptions.inSampleSize = scaleFactor;
                    bmOptions.inPurgeable = true;
                    Bitmap bitmap = BitmapFactory.decodeFile(imagePathKYC, bmOptions);
                    kyc_Doc_img.setImageBitmap(bitmap);
                } else if (imageView.equals("Address_Proof_img")) {
                    int targetW = Address_Proof_img.getWidth();
                    int targetH = Address_Proof_img.getHeight();
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(imagePathAddress_Proof, bmOptions);
                    int photoW = bmOptions.outWidth;
                    int photoH = bmOptions.outHeight;
                    int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
                    bmOptions.inJustDecodeBounds = false;
                    bmOptions.inSampleSize = scaleFactor;
                    bmOptions.inPurgeable = true;
                    Bitmap bitmap = BitmapFactory.decodeFile(imagePathAddress_Proof, bmOptions);
                    Address_Proof_img.setImageBitmap(bitmap);
                } else if (imageView.equals("Passport_Size_img")) {
                    int targetW = Passport_Size_img.getWidth();
                    int targetH = Passport_Size_img.getHeight();
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(imagePathPassportSizephoto, bmOptions);
                    int photoW = bmOptions.outWidth;
                    int photoH = bmOptions.outHeight;
                    int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
                    bmOptions.inJustDecodeBounds = false;
                    bmOptions.inSampleSize = scaleFactor;
                    bmOptions.inPurgeable = true;
                    Bitmap bitmap = BitmapFactory.decodeFile(imagePathAddress_Proof, bmOptions);
                    Passport_Size_img.setImageBitmap(bitmap);

                } else if (imageView.equals("Driving_License_img")) {
                    int targetW = Driving_License_img.getWidth();
                    int targetH = Driving_License_img.getHeight();
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(imagePathPassportSizephoto, bmOptions);
                    int photoW = bmOptions.outWidth;
                    int photoH = bmOptions.outHeight;
                    int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
                    bmOptions.inJustDecodeBounds = false;
                    bmOptions.inSampleSize = scaleFactor;
                    bmOptions.inPurgeable = true;
                    Bitmap bitmap = BitmapFactory.decodeFile(imagePathPassportSizephoto, bmOptions);
                    Passport_Size_img.setImageBitmap(bitmap);
                } else if (imageView.equals("Voter_Id_img")) {
                    int targetW = Voter_Id_img.getWidth();
                    int targetH = Voter_Id_img.getHeight();
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(imagePathVoter_Id, bmOptions);
                    int photoW = bmOptions.outWidth;
                    int photoH = bmOptions.outHeight;
                    int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
                    bmOptions.inJustDecodeBounds = false;
                    bmOptions.inSampleSize = scaleFactor;
                    bmOptions.inPurgeable = true;
                    Bitmap bitmap = BitmapFactory.decodeFile(imagePathVoter_Id, bmOptions);
                    Voter_Id_img.setImageBitmap(bitmap);

                } else if (imageView.equals("Adharcard_img_fastag")) {
                    int targetW = Adharcard_img_fastag.getWidth();
                    int targetH = Adharcard_img_fastag.getHeight();
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(imagePathVoter_Id, bmOptions);
                    int photoW = bmOptions.outWidth;
                    int photoH = bmOptions.outHeight;
                    int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
                    bmOptions.inJustDecodeBounds = false;
                    bmOptions.inSampleSize = scaleFactor;
                    bmOptions.inPurgeable = true;
                    Bitmap bitmap = BitmapFactory.decodeFile(imagePathAdharcard, bmOptions);
                    Adharcard_img_fastag.setImageBitmap(bitmap);

                }
            }
        }
        if (requestCode == GALLERY_INTENT_CALLED && data != null) {
            Uri uri = data.getData();
            if (imageView.equals("Registration_Certificate_img")) {
                imagePathRegistration_C = RealPathUtil.getPath(mContext, uri);
                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(imagePathRegistration_C);
                Registration_Certificate_img.setImageBitmap(bitmap);
                options.inSampleSize = 4;
                Log.d("imagePathRegistrationC>>>", imagePathRegistration_C);
            } else if (imageView.equals("kyc_Doc_img")) {
                imagePathKYC = RealPathUtil.getPath(mContext, uri);
                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(imagePathKYC);
                kyc_Doc_img.setImageBitmap(bitmap);
                options.inSampleSize = 4;
                Log.d("imagePathPanCard>>>", imagePathKYC);
            } else if (imageView.equals("Address_Proof_img")) {

                imagePathAddress_Proof = RealPathUtil.getPath(mContext, uri);
                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(imagePathAddress_Proof);
                Address_Proof_img.setImageBitmap(bitmap);
                options.inSampleSize = 4;
                Log.d("imagePathAddress>>>", imagePathAddress_Proof);


            } else if (imageView.equals("Passport_Size_img")) {
                imagePathPassportSizephoto = RealPathUtil.getPath(mContext, uri);
                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(imagePathPassportSizephoto);
                Passport_Size_img.setImageBitmap(bitmap);
                options.inSampleSize = 4;
                Log.d("Passport_Size_img>>>", imagePathPassportSizephoto);

            } else if (imageView.equals("Driving_License_img")) {
                imagePathDriving_License = RealPathUtil.getPath(mContext, uri);
                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(imagePathDriving_License);
                Driving_License_img.setImageBitmap(bitmap);
                options.inSampleSize = 4;
                Log.d("Driving_License_img>>>", imagePathDriving_License);

            } else if (imageView.equals("Voter_Id_img")) {
                imagePathVoter_Id = RealPathUtil.getPath(mContext, uri);
                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(imagePathVoter_Id);
                Voter_Id_img.setImageBitmap(bitmap);
                options.inSampleSize = 4;
                Log.d("Voter_Id_img>>>", imagePathVoter_Id);

            } else if (imageView.equals("Adharcard_img_fastag")) {
                imagePathAdharcard = RealPathUtil.getPath(mContext, uri);
                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(imagePathAdharcard);
                Adharcard_img_fastag.setImageBitmap(bitmap);
                options.inSampleSize = 4;
                Log.d("Adharcard_img_fastag>>>", imagePathAdharcard);
            }
        }
    }

}
