package b2infosoft.achariya.com.addley.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import b2infosoft.achariya.com.BuildConfig;
import b2infosoft.achariya.com.R;
import b2infosoft.achariya.com.global.AppGlobal;
import b2infosoft.achariya.com.sharedPreference.SessionManager;
import b2infosoft.achariya.com.useful.RealPathUtil;
import b2infosoft.achariya.com.useful.UtilityMethod;
import b2infosoft.achariya.com.webservice.WebServiceCaller;


public class UploadDocsActivity extends AppCompatActivity implements View.OnClickListener {


    TextView Adharcard_txt, PanCard_txt, PoliceVerification_txt, Passport_Size_txt, Education_Proof_txt, Bank_Verification_txt, Fees, Fees_Text;
    ImageView Adharcard_img, PanCard_img, PoliceVerification_img, Passport_Size_img, EducationProof_img, Bank_Verification_img;
    Uri imageUri = null;

    public static ImageView showImg = null;
    // CameraPhotoCapture CameraActivity = null;
    Context mContext;
    String imagePathAadhar = "", imagePathPanCard = "", imagePathPoliceVerification = "", imagePathPassportSizephoto = "",
            imagePathEducation_Proof = "", imagePathBank_verification = "";
    String imageView = "";

    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private boolean sentToSettings = false;
    private SharedPreferences permissionStatus;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 101;
    private static final int GALLERY_INTENT_CALLED = 201;
    Toolbar toolbar;
    TextView toolbar_title;
    String fromWhere = "", Name = "", Email = "", Mobile = "", Address = "", FeesTxt = "";
    Button btnUpload;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__upload__doc);
        mContext = UploadDocsActivity.this;
        initview();
    }

    private void initview() {
        sessionManager = new SessionManager(mContext);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("UPLOAD DOCUMENT");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Drawable upArrow = ContextCompat.getDrawable(mContext, R.drawable.abc_ic_ab_back_material);

        upArrow.setColorFilter(ContextCompat.getColor(mContext, R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);

        Adharcard_txt = (TextView) findViewById(R.id.Adharcard_txt);
        PanCard_txt = (TextView) findViewById(R.id.PanCard_txt);
        PoliceVerification_txt = (TextView) findViewById(R.id.PoliceVerification_txt);
        Passport_Size_txt = (TextView) findViewById(R.id.Passport_Size_txt);
        Education_Proof_txt = (TextView) findViewById(R.id.Education_Proof_txt);
        Bank_Verification_txt = (TextView) findViewById(R.id.Bank_Verification_txt);
        Fees = (TextView) findViewById(R.id.Fees);
        Fees_Text = (TextView) findViewById(R.id.Fees_Text);
        Adharcard_img = (ImageView) findViewById(R.id.Adharcard_img);
        PanCard_img = (ImageView) findViewById(R.id.PanCard_img);
        PoliceVerification_img = (ImageView) findViewById(R.id.PoliceVerification_img);
        Passport_Size_img = (ImageView) findViewById(R.id.Passport_Size_img);
        EducationProof_img = (ImageView) findViewById(R.id.EducationProof_img);
        Bank_Verification_img = (ImageView) findViewById(R.id.Bank_Verification_img);
        btnUpload = (Button) findViewById(R.id.btnUpload);
        Adharcard_img.setOnClickListener(this);
        PanCard_img.setOnClickListener(this);
        PoliceVerification_img.setOnClickListener(this);
        Passport_Size_img.setOnClickListener(this);
        EducationProof_img.setOnClickListener(this);
        Bank_Verification_img.setOnClickListener(this);
        btnUpload.setOnClickListener(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        if (intent != null) {
            Name = intent.getStringExtra("Name");
            Mobile = intent.getStringExtra("Mobile");
            Address = intent.getStringExtra("Address");
            Email = intent.getStringExtra("Email");
            fromWhere = intent.getStringExtra("fromWhere");
            FeesTxt = intent.getStringExtra("fees");
            Log.d("UserInfo>>>", "" + fromWhere + "::" + Name + "::" + Mobile + "::" + Address + "::" + Email + "::" + FeesTxt);
        }

        if (!AppGlobal.Aadhar_card.equals("")) {
            Glide.with(this).load(AppGlobal.AddleyImageUrl + AppGlobal.Aadhar_card)
                    .into(Adharcard_img);
            //imagePathAadhar = AppGlobal.Aadhar_card;
        }
        if (!AppGlobal.Pan_card.equals("")) {
            Glide.with(this).load(AppGlobal.AddleyImageUrl + AppGlobal.Pan_card).into(PanCard_img);
            //imagePathPanCard = AppGlobal.Pan_card;
        }
        if (!AppGlobal.Police_ver.equals("")) {
            Glide.with(this).load(AppGlobal.AddleyImageUrl + AppGlobal.Police_ver).into(PoliceVerification_img);
            //imagePathPoliceVerification = AppGlobal.Police_ver;
        }

        if (!AppGlobal.Passport_pic.equals("")) {
            Glide.with(this).load(AppGlobal.AddleyImageUrl + AppGlobal.Passport_pic).into(Passport_Size_img);
            // imagePathPassportSizephoto = AppGlobal.Passport_pic;
        }

        if (!AppGlobal.Eduction_proof.equals("")) {
            Glide.with(this).load(AppGlobal.AddleyImageUrl + AppGlobal.Eduction_proof).into(EducationProof_img);
            // imagePathEducation_Proof = AppGlobal.Eduction_proof;
        }
        if (!AppGlobal.Bank_passbook.equals("")) {
            Glide.with(this).load(AppGlobal.AddleyImageUrl + AppGlobal.Bank_passbook).into(Bank_Verification_img);
            // imagePathBank_verification = AppGlobal.Bank_passbook;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.Adharcard_img:
                imageView = "Adharcard_img";
                permissionAccess();
                break;

            case R.id.PanCard_img:
                imageView = "PanCard_img";
                permissionAccess();
                break;

            case R.id.PoliceVerification_img:
                imageView = "PoliceVerification_img";
                permissionAccess();
                break;

            case R.id.Passport_Size_img:
                imageView = "Passport_Size_img";
                permissionAccess();
                break;
            case R.id.EducationProof_img:
                imageView = "EducationProof_img";
                permissionAccess();
                break;


            case R.id.Bank_Verification_img:
                imageView = "Bank_Verification_img";
                permissionAccess();
                break;
            case R.id.btnUpload:

             /*   if (!imagePathBank_verification.trim().equals("") && !imagePathPoliceVerification.trim().equals("") && !imagePathPanCard.trim().equals("")
                        && !imagePathEducation_Proof.trim().equals("") && !imagePathEducation_Proof.trim().equals("") && !imagePathAadhar.trim().equals("")) {
*/
                if (AppGlobal.UserType.equals("New")) {
                    uploadData();

                } else {
                    updateData();
                }
               /* } else {
                    UtilityMethod.showAlertBox(mContext, "Please Select All Documents.");
                }*/

                break;
        }
    }

    private void updateData() {

        WebServiceCaller caller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please Wait...", true) {
            @Override
            public void handleResponse(String response) {
                String successStr = "";
                Log.d("Respose---->>>>", response);
                try {
                    JSONObject mainObj = new JSONObject(response);
                    if (mainObj.getString("success").equals("true")) {
                        UtilityMethod.showAlertBoxwithIntent(mContext, "Data Updated Successfully", Thank_you_Activity.class);
                    } else {
                        UtilityMethod.showAlertBox(mContext, mainObj.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
        entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        entityBuilder.addTextBody("name", Name);
        entityBuilder.addTextBody("mobile", Mobile);
        entityBuilder.addTextBody("email", Email);
        entityBuilder.addTextBody("address", Address);
        entityBuilder.addTextBody("fees", FeesTxt);
        entityBuilder.addTextBody("user_type_id", "" + fromWhere);
        entityBuilder.addTextBody("user_id", "" + sessionManager.getSessionValue(SessionManager.KEY_ID));
        if (imagePathAadhar.equals("")) {
        } else {
            Log.d("imagePathAadhar>>>>", "" + new File(imagePathAadhar).getName());
            entityBuilder.addBinaryBody("aadhar_card", new File(imagePathAadhar), ContentType.create("image/jpeg"), new File(imagePathAadhar).getName());
        }
        if (imagePathPassportSizephoto.equals("")) {
        } else {
            Log.d("passport_pic>>>>", "" + new File(imagePathPassportSizephoto).getName());
            entityBuilder.addBinaryBody("passport_pic", new File(imagePathPassportSizephoto), ContentType.create("image/jpeg"), new File(imagePathPassportSizephoto).getName());
        }
        if (imagePathEducation_Proof.equals("")) {
        } else {
            Log.d("eduction_proof>>>>", "" + new File(imagePathEducation_Proof).getName());
            entityBuilder.addBinaryBody("eduction_proof", new File(imagePathEducation_Proof), ContentType.create("image/jpeg"), new File(imagePathEducation_Proof).getName());
        }
        if (imagePathPanCard.equals("")) {
        } else {
            Log.d("pan_card>>>>", "" + new File(imagePathPanCard).getName());
            entityBuilder.addBinaryBody("pan_card", new File(imagePathPanCard), ContentType.create("image/jpeg"), new File(imagePathPanCard).getName());
        }
        if (imagePathPoliceVerification.equals("")) {
        } else {
            Log.d("police_ver>>>>", "" + new File(imagePathPoliceVerification).getName());
            entityBuilder.addBinaryBody("police_ver", new File(imagePathPoliceVerification), ContentType.create("image/jpeg"), new File(imagePathPoliceVerification).getName());
        }
        if (imagePathBank_verification.equals("")) {
        } else {
            Log.d("bank_passbook>>>>", "" + new File(imagePathBank_verification).getName());
            entityBuilder.addBinaryBody("bank_passbook", new File(imagePathBank_verification), ContentType.create("image/jpeg"), new File(imagePathBank_verification).getName());
        }
        HttpEntity entity = entityBuilder.build();
        caller.addEntity(entity);
        caller.execute(AppGlobal.AddleyUpdateUserRegister);

    }

    public void uploadData() {
        WebServiceCaller caller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please Wait...", true) {
            @Override
            public void handleResponse(String response) {
                String successStr = "";
                Log.d("Respose---->>>>", response);
                try {
                    JSONObject mainObj = new JSONObject(response);
                    if (mainObj.getString("success").equals("true")) {
                        UtilityMethod.showAlertBoxwithIntent(mContext, "Data Uploaded Successfully", Thank_you_Activity.class);
                    } else {
                        UtilityMethod.showAlertBox(mContext, mainObj.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
        entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        entityBuilder.addTextBody("name", Name);
        entityBuilder.addTextBody("mobile", Mobile);
        entityBuilder.addTextBody("email", Email);
        entityBuilder.addTextBody("address", Address);
        entityBuilder.addTextBody("fees", FeesTxt);
        entityBuilder.addTextBody("user_type_id", "" + fromWhere);
        entityBuilder.addTextBody("user_id", "" + sessionManager.getSessionValue(SessionManager.KEY_ID));
        if (imagePathAadhar.equals("")) {
        } else {
            Log.d("imagePathAadhar>>>>", "" + new File(imagePathAadhar).getName());
            entityBuilder.addBinaryBody("aadhar_card", new File(imagePathAadhar), ContentType.create("image/jpeg"), new File(imagePathAadhar).getName());
        }
        if (imagePathPassportSizephoto.equals("")) {
        } else {
            Log.d("passport_pic>>>>", "" + new File(imagePathPassportSizephoto).getName());
            entityBuilder.addBinaryBody("passport_pic", new File(imagePathPassportSizephoto), ContentType.create("image/jpeg"), new File(imagePathPassportSizephoto).getName());
        }
        if (imagePathEducation_Proof.equals("")) {
        } else {
            Log.d("eduction_proof>>>>", "" + new File(imagePathEducation_Proof).getName());
            entityBuilder.addBinaryBody("eduction_proof", new File(imagePathEducation_Proof), ContentType.create("image/jpeg"), new File(imagePathEducation_Proof).getName());
        }
        if (imagePathPanCard.equals("")) {
        } else {
            Log.d("pan_card>>>>", "" + new File(imagePathPanCard).getName());
            entityBuilder.addBinaryBody("pan_card", new File(imagePathPanCard), ContentType.create("image/jpeg"), new File(imagePathPanCard).getName());
        }
        if (imagePathPoliceVerification.equals("")) {
        } else {
            Log.d("police_ver>>>>", "" + new File(imagePathPoliceVerification).getName());
            entityBuilder.addBinaryBody("police_ver", new File(imagePathPoliceVerification), ContentType.create("image/jpeg"), new File(imagePathPoliceVerification).getName());
        }
        if (imagePathBank_verification.equals("")) {
        } else {
            Log.d("bank_passbook>>>>", "" + new File(imagePathBank_verification).getName());
            entityBuilder.addBinaryBody("bank_passbook", new File(imagePathBank_verification), ContentType.create("image/jpeg"), new File(imagePathBank_verification).getName());
        }
        HttpEntity entity = entityBuilder.build();
        caller.addEntity(entity);
        caller.execute(AppGlobal.AddleyRegisterUser);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == EXTERNAL_STORAGE_PERMISSION_CONSTANT) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //The External Storage Write Permission is granted to you... Continue your left job...
                selectImage();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(UploadDocsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(UploadDocsActivity.this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(UploadDocsActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    //Show Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(UploadDocsActivity.this);
                    builder.setTitle("Need Storage and Camera Permission");
                    builder.setMessage("This app needs storage and Camera permission");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            ActivityCompat.requestPermissions(UploadDocsActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    Toast.makeText(getBaseContext(), "Unable to get Permission", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public void permissionAccess() {
        if (ActivityCompat.checkSelfPermission(UploadDocsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(UploadDocsActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(UploadDocsActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(UploadDocsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(UploadDocsActivity.this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(UploadDocsActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(UploadDocsActivity.this);
                builder.setTitle("Need Storage and Camera Permission");
                builder.setMessage("This app needs storage and Camera permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(UploadDocsActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (permissionStatus.getBoolean(Manifest.permission.WRITE_EXTERNAL_STORAGE, false) || permissionStatus.getBoolean(Manifest.permission.CAMERA, false) || permissionStatus.getBoolean(Manifest.permission.READ_EXTERNAL_STORAGE, false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(UploadDocsActivity.this);
                builder.setTitle("Need Storage and Camera Permission");
                builder.setMessage("This app needs storage and Camera permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getBaseContext(), "Go to Permissions to Grant Storage and Camera", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(UploadDocsActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
            }
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.WRITE_EXTERNAL_STORAGE, true);
            editor.putBoolean(Manifest.permission.CAMERA, true);
            editor.putBoolean(Manifest.permission.READ_EXTERNAL_STORAGE, true);
            editor.commit();

        } else {
            //You already have the permission, just go ahead.
            selectImage();

        }
    }


    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Upload Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                        }
                        if (photoFile != null) {
                            Uri photoURI;
                            if (Build.VERSION.SDK_INT >= 24) {
                                photoURI = FileProvider.getUriForFile(UploadDocsActivity.this,
                                        BuildConfig.APPLICATION_ID + ".provider", photoFile);
                            } else {
                                photoURI = Uri.fromFile(photoFile);
                            }

                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                            startActivityForResult(takePictureIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);

                        }
                    }
                } else if (items[item].equals("Choose from Gallery")) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                        Intent i = new Intent(
                                Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, GALLERY_INTENT_CALLED);
                    } else {
                        Intent i = new Intent(
                                Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, GALLERY_INTENT_CALLED);
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        if (imageView.equals("Adharcard_img")) {
            imagePathAadhar = image.getAbsolutePath();
            Log.e("Getpath", "Cool" + imagePathAadhar);
        } else if (imageView.equals("PanCard_img")) {
            imagePathPanCard = image.getAbsolutePath();
            Log.e("Getpath", "Cool" + imagePathPanCard);
        } else if (imageView.equals("PoliceVerification_img")) {
            imagePathPoliceVerification = image.getAbsolutePath();
            Log.e("Getpath", "Cool" + imagePathPoliceVerification);
        } else if (imageView.equals("Passport_Size_img")) {
            imagePathPassportSizephoto = image.getAbsolutePath();
            Log.e("Getpath", "Cool" + imagePathPassportSizephoto);
        } else if (imageView.equals("EducationProof_img")) {
            imagePathEducation_Proof = image.getAbsolutePath();
            Log.e("Getpath", "cool" + imagePathEducation_Proof);
        } else if (imageView.equals("Bank_Verification_img")) {
            imagePathBank_verification = image.getAbsolutePath();
            Log.e("Getpath", "cool" + imagePathBank_verification);
        }


        return image;
    }


    @SuppressLint("LongLogTag")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Activity activity = (Activity) mContext;
                if (imageView.equals("Adharcard_img")) {
                    int targetW = Adharcard_img.getWidth();
                    int targetH = Adharcard_img.getHeight();

                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(imagePathAadhar, bmOptions);
                    int photoW = bmOptions.outWidth;
                    int photoH = bmOptions.outHeight;

                    int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

                    bmOptions.inJustDecodeBounds = false;
                    bmOptions.inSampleSize = scaleFactor;
                    bmOptions.inPurgeable = true;

                    Bitmap bitmap = BitmapFactory.decodeFile(imagePathAadhar, bmOptions);
                    Adharcard_img.setImageBitmap(bitmap);

                } else if (imageView.equals("PanCard_img")) {
                    int targetW = PanCard_img.getWidth();
                    int targetH = PanCard_img.getHeight();
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(imagePathPanCard, bmOptions);
                    int photoW = bmOptions.outWidth;
                    int photoH = bmOptions.outHeight;
                    int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
                    bmOptions.inJustDecodeBounds = false;
                    bmOptions.inSampleSize = scaleFactor;
                    bmOptions.inPurgeable = true;

                    Bitmap bitmap = BitmapFactory.decodeFile(imagePathPanCard, bmOptions);
                    PanCard_img.setImageBitmap(bitmap);
                } else if (imageView.equals("PoliceVerification_img")) {
                    int targetW = PoliceVerification_img.getWidth();
                    int targetH = PoliceVerification_img.getHeight();
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(imagePathPoliceVerification, bmOptions);
                    int photoW = bmOptions.outWidth;
                    int photoH = bmOptions.outHeight;
                    int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
                    bmOptions.inJustDecodeBounds = false;
                    bmOptions.inSampleSize = scaleFactor;
                    bmOptions.inPurgeable = true;

                    Bitmap bitmap = BitmapFactory.decodeFile(imagePathPoliceVerification, bmOptions);
                    PoliceVerification_img.setImageBitmap(bitmap);
                } else if (imageView.equals("Passport_Size_img")) {
                    int targetW = Passport_Size_img.getWidth();
                    int targetH = Passport_Size_img.getHeight();
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(imagePathPassportSizephoto, bmOptions);
                    int photoW = bmOptions.outWidth;
                    int photoH = bmOptions.outHeight;
                    int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
                    bmOptions.inJustDecodeBounds = false;
                    bmOptions.inSampleSize = scaleFactor;
                    bmOptions.inPurgeable = true;
                    Bitmap bitmap = BitmapFactory.decodeFile(imagePathPassportSizephoto, bmOptions);
                    Passport_Size_img.setImageBitmap(bitmap);

                } else if (imageView.equals("EducationProof_img")) {
                    int targetW = EducationProof_img.getWidth();
                    int targetH = EducationProof_img.getHeight();
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(imagePathEducation_Proof, bmOptions);
                    int photoW = bmOptions.outWidth;
                    int photoH = bmOptions.outHeight;
                    int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
                    bmOptions.inJustDecodeBounds = false;
                    bmOptions.inSampleSize = scaleFactor;
                    bmOptions.inPurgeable = true;

                    Bitmap bitmap = BitmapFactory.decodeFile(imagePathEducation_Proof, bmOptions);
                    EducationProof_img.setImageBitmap(bitmap);
                } else if (imageView.equals("Bank_Verification_img")) {
                    int targetW = Bank_Verification_img.getWidth();
                    int targetH = Bank_Verification_img.getHeight();
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(imagePathBank_verification, bmOptions);
                    int photoW = bmOptions.outWidth;
                    int photoH = bmOptions.outHeight;
                    int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
                    bmOptions.inJustDecodeBounds = false;
                    bmOptions.inSampleSize = scaleFactor;
                    bmOptions.inPurgeable = true;

                    Bitmap bitmap = BitmapFactory.decodeFile(imagePathBank_verification, bmOptions);
                    Bank_Verification_img.setImageBitmap(bitmap);
                }

            }
        }
        if (requestCode == GALLERY_INTENT_CALLED && data != null) {
            Uri uri = data.getData();
            if (imageView.equals("Adharcard_img")) {
                imagePathAadhar = RealPathUtil.getPath(mContext, uri);
                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(imagePathAadhar);
                Adharcard_img.setImageBitmap(bitmap);
                options.inSampleSize = 4;
                Log.d("imagepath>>>", imagePathAadhar);
            } else if (imageView.equals("PanCard_img")) {
                imagePathPanCard = RealPathUtil.getPath(mContext, uri);
                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(imagePathPanCard);
                PanCard_img.setImageBitmap(bitmap);
                options.inSampleSize = 4;
                Log.d("imagePathPanCard>>>", imagePathPanCard);
            } else if (imageView.equals("PoliceVerification_img")) {
                imagePathPoliceVerification = RealPathUtil.getPath(mContext, uri);
                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(imagePathPoliceVerification);
                PoliceVerification_img.setImageBitmap(bitmap);
                options.inSampleSize = 4;
                Log.d("imagePathPoliceVerification>>>", imagePathPoliceVerification);
            } else if (imageView.equals("Passport_Size_img")) {
                imagePathPassportSizephoto = RealPathUtil.getPath(mContext, uri);
                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(imagePathPassportSizephoto);
                Passport_Size_img.setImageBitmap(bitmap);
                options.inSampleSize = 4;
                Log.d("imagePathPassportSizephoto>>>", imagePathPassportSizephoto);
            } else if (imageView.equals("EducationProof_img")) {
                imagePathEducation_Proof = RealPathUtil.getPath(mContext, uri);
                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(imagePathEducation_Proof);
                EducationProof_img.setImageBitmap(bitmap);
                options.inSampleSize = 4;
                Log.d(" imagePathEducation_Proof>>>", imagePathEducation_Proof);
            } else if (imageView.equals("Bank_Verification_img")) {
                imagePathBank_verification = RealPathUtil.getPath(mContext, uri);
                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(imagePathBank_verification);
                Bank_Verification_img.setImageBitmap(bitmap);
                options.inSampleSize = 4;
                Log.d(" imagePathBank_verification>>>", imagePathBank_verification);
            }
        }
    }

}
