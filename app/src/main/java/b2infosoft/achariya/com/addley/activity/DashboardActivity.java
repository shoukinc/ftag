package b2infosoft.achariya.com.addley.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONException;
import org.json.JSONObject;

import b2infosoft.achariya.com.R;
import b2infosoft.achariya.com.activity.AcharyaDeshBoardActivity;
import b2infosoft.achariya.com.global.AppGlobal;
import b2infosoft.achariya.com.sharedPreference.SessionManager;
import b2infosoft.achariya.com.useful.UtilityMethod;
import b2infosoft.achariya.com.webservice.WebServiceCaller;


public class DashboardActivity extends AppCompatActivity implements View.OnClickListener {


    Context mContext;
    Toolbar toolbar;
    TextView toolbar_title;
    LinearLayout layoutSolar, layoutBankBc, layoutShopping, layoutJobs, layoutBillPayment, layoutFastTag, layoutPaytm, layoutRkcl, layoutEmitra;
    boolean doubleBackToExitPressedOnce = false;

    SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        mContext = DashboardActivity.this;
        initView();

    }

    private void initView() {

        sessionManager = new SessionManager(mContext);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("DESHBOARD");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Drawable upArrow = ContextCompat.getDrawable(mContext, R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(ContextCompat.getColor(mContext, R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        layoutRkcl = (LinearLayout) findViewById(R.id.layoutRkcl);
        layoutEmitra = (LinearLayout) findViewById(R.id.layoutEmitra);
        layoutSolar = (LinearLayout) findViewById(R.id.layoutSolar);
        layoutBankBc = (LinearLayout) findViewById(R.id.layoutBankBc);
        layoutShopping = (LinearLayout) findViewById(R.id.layoutShopping);
        layoutJobs = (LinearLayout) findViewById(R.id.layoutJobs);
        layoutBillPayment = (LinearLayout) findViewById(R.id.layoutBillPayment);
        layoutFastTag = (LinearLayout) findViewById(R.id.layoutFastTag);
        layoutPaytm = (LinearLayout) findViewById(R.id.layoutPaytm);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        layoutEmitra.setOnClickListener(this);
        layoutRkcl.setOnClickListener(this);
        layoutSolar.setOnClickListener(this);
        layoutBankBc.setOnClickListener(this);
        layoutShopping.setOnClickListener(this);
        layoutJobs.setOnClickListener(this);
        layoutBillPayment.setOnClickListener(this);
        layoutFastTag.setOnClickListener(this);
        layoutPaytm.setOnClickListener(this);

       // Log.d("SessionID", ""+sessionManager.getSessionValue(SessionManager.KEY_ID));


    }

    @Override
    public void onClick(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.layoutEmitra:


                checkUserService("2", "Emitra");

                break;
            case R.id.layoutRkcl:

                checkUserService("3", "Rkcl");
                break;
            case R.id.layoutFastTag:

                //checkUserService("5", "FastTag");
                startActivity(new Intent(mContext, AcharyaDeshBoardActivity.class));
                break;
            case R.id.layoutShopping:
                //startActivity(new Intent(mContext, Main2.class));
                break;
            case R.id.layoutBankBc:

                checkUserService("4", "BankBc");
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


    public void checkUserService(String service_type, final String fromWhere) {
        WebServiceCaller serviceCaller = new WebServiceCaller(WebServiceCaller.POST_TASK, mContext, "Please wait...", true) {
            @Override
            public void handleResponse(String response) {
                Log.d("Response>>>>", response);
                Intent intent = null;
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {
                        if (jsonObject.getString("data").equals("1")) {
                            AppGlobal.Name = jsonObject.getString("name");
                            AppGlobal.Mobile = jsonObject.getString("mobile");
                            AppGlobal.Address = jsonObject.getString("address");
                            AppGlobal.Fees = jsonObject.getString("fees");
                            AppGlobal.Aadhar_card = jsonObject.getString("aadhar_card");
                            AppGlobal.Passport_pic = jsonObject.getString("passport_pic");
                            AppGlobal.Pan_card = jsonObject.getString("pan_card");
                            AppGlobal.Police_ver = jsonObject.getString("police_ver");
                            AppGlobal.Bank_passbook = jsonObject.getString("bank_passbook");
                            AppGlobal.Reg_certificate = jsonObject.getString("reg_certificate");
                            AppGlobal.Address_Proof = jsonObject.getString("address_Proof");
                            AppGlobal.Photo = jsonObject.getString("photo");
                            AppGlobal.Driving_license = jsonObject.getString("driving_license");
                            AppGlobal.Voter_card = jsonObject.getString("voter_card");
                            AppGlobal.Email = jsonObject.getString("email");
                            AppGlobal.Eduction_proof = jsonObject.getString("eduction_proof");
                            AppGlobal.Kyc_document = jsonObject.getString("kyc_document");
                            AppGlobal.UserType = "Exist";
                        } else {
                            AppGlobal.Name = sessionManager.getSessionValue(SessionManager.KEY_NAME);
                            AppGlobal.Mobile = sessionManager.getSessionValue(SessionManager.KEY_Mobile);
                            AppGlobal.Address = "";
                            AppGlobal.Fees = "";
                            AppGlobal.Aadhar_card = "";
                            AppGlobal.Passport_pic = "";
                            AppGlobal.Pan_card = "";
                            AppGlobal.Police_ver = "";
                            AppGlobal.Bank_passbook = "";
                            AppGlobal.Reg_certificate = "";
                            AppGlobal.Address_Proof = "";
                            AppGlobal.Photo = "";
                            AppGlobal.Driving_license = "";
                            AppGlobal.Voter_card = "";
                            AppGlobal.Email = "";
                            AppGlobal.Eduction_proof = "";
                            AppGlobal.Kyc_document = "";
                            AppGlobal.UserType = "New";
                        }
                        if (fromWhere.equals("Emitra")) {
                            intent = new Intent(mContext, SignUpActivity.class);
                            intent.putExtra("fromWhere", "2");
                            startActivity(intent);
                        } else if (fromWhere.equals("Rkcl")) {
                            intent = new Intent(mContext, SignUpActivity.class);
                            intent.putExtra("fromWhere", "3");
                            startActivity(intent);
                        } else if (fromWhere.equals("FastTag")) {
                            startActivity(new Intent(mContext, FastTagSignUpActivity.class));
                        } else if (fromWhere.equals("BankBc")) {
                            intent = new Intent(mContext, SignUpActivity.class);
                            intent.putExtra("fromWhere", "4");
                            startActivity(intent);
                        }
                    } else {
                        UtilityMethod.showAlertBox(mContext, "Getting User Info Failed.");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        serviceCaller.addNameValuePair("user_id", sessionManager.getSessionValue(SessionManager.KEY_ID));
        serviceCaller.addNameValuePair("service_type", service_type);
        serviceCaller.execute(AppGlobal.AddleyCheckService);

    }


}
