package b2infosoft.achariya.com.addley.activity;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import b2infosoft.achariya.com.R;
import b2infosoft.achariya.com.useful.UtilityMethod;


public class Thank_you_Activity extends AppCompatActivity {

    Context mContext;
    Toolbar toolbar;
    TextView toolbar_title;
    ImageView img_Addley;
    Button btnBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you_);

        mContext = Thank_you_Activity.this;

        img_Addley = (ImageView) findViewById(R.id.img_Addley);
        btnBack = (Button) findViewById(R.id.btnBack);


        initview();
    }

    private void initview() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setText("THANK YOU");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Drawable upArrow = ContextCompat.getDrawable(mContext, R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(ContextCompat.getColor(mContext, R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilityMethod.showAlertBoxwithIntent(mContext, "Thank you for visiting this app", DashboardActivity.class);
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UtilityMethod.showAlertBoxwithIntent(mContext, "Thank you for visiting this app", DashboardActivity.class);

            }
        });

    }
}
