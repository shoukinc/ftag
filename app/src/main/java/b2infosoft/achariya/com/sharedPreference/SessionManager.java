package b2infosoft.achariya.com.sharedPreference;

import android.content.Context;
import android.content.SharedPreferences;


public class SessionManager {

    Context mContext;
    static final String MyPREFERENCES = "MyPrefs";

    public static SharedPreferences sharedPreferences;
    public static SharedPreferences.Editor editor;
    private static final String IS_LOGIN = "IsLoggedIn";
    public static String Key_UserGroupID = "gID";


    public static final String KEY_NAME = "name";
    public static final String KEY_Mobile = "mobile";
    public static final String KEY_ID = "id";



    public SessionManager(Context mContext) {
        this.mContext = mContext;

        sharedPreferences = mContext.getSharedPreferences(MyPREFERENCES, mContext.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }


    public void createLoginSession(String name, String mobile,String id){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing name in pref
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_Mobile, mobile);
        editor.putString(KEY_ID, id);
        editor.apply();
    }

    public String getSessionValue(String key){
        String value= sharedPreferences.getString(key,null);
        return  value;
    }


    public void setValueSession(String key, String value) {
        editor.putString(key, value);
        editor.commit();

    }

    public void clearUserPreference(Context mContext) {

        this.mContext = mContext;
        editor.clear();
        editor.commit();
    }

    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

    }

    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(IS_LOGIN, false);
    }


}
