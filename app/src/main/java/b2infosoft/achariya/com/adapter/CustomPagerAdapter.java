package b2infosoft.achariya.com.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import b2infosoft.achariya.com.R;


/**
 * Created by Microsoft on 23-Sep-17.
 */

public class CustomPagerAdapter extends PagerAdapter {

    private Activity activity;
    ArrayList<Integer> bannerList;
    Context context;

    public CustomPagerAdapter(Activity activity, ArrayList<Integer> bannerList, Context ctx) {

        this.activity = activity;
        this.bannerList = bannerList;
        context = ctx;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        LayoutInflater inflater = ((Activity) activity).getLayoutInflater();
        View viewItem = inflater.inflate(R.layout.acharya_customer_layoutimage, container, false);
        ImageView imageView = (ImageView) viewItem.findViewById(R.id.image);
        Glide.with(context)
                .load(bannerList.get(position))
                .into(imageView);
        ((ViewPager) container).addView(viewItem);
        return viewItem;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return bannerList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        // TODO Auto-generated method stub
        return view == ((View) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }
}

